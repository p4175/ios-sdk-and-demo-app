
# StimshopSDK

## Description

StimshopSDK implements Stimshop's signal detection protocol.

This SDK scans audio frequencies in the environment through the microphone to trigger events.

This SDK is developped in `Kotlin KMM`.


## Setup

Copy the `.xcframework` folder (located in iosApp/Frameworks/) inside your project.

In your project settings, select your app target, and add the `.xcframework` in the **Frameworks, Libraries, and Embedded Content** :

Create `Stimshop.plist` file in the root folder of your project :

````xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
    <dict>
        <key>stimshop_apiKey</key>
        <string>##### YOUR API KEY HERE #####</string>
        <key>stimshop_apiToken</key>
        <string>##### YOUR TOKEN HERE #####</string>
        <key>stimshop_channels</key>
        <array>
            <integer>1</integer>
            <integer>2</integer>
        </array>
        <key>stimshop_detection_mode</key>
        <integer>0</integer>
    </dict>
</plist>
````

We assume you already have your API key, allowing you to use Stimshop services.

You will also need an access token generated from [ucheck.in](https://ucheck.in) application from your user account

Both **API Key** and **Token** will be used in the `Stimshop.plist` file.

You can configure the channels you want to use:

- 1: from 17 to 18KHz
- 2: from 18 to 19KHz
- 3: from 19 to 20KHz
- 4: from 20 to 21KHz

and the initial detection mode:

- 0: ucheck-in mode
- 1: wi-us mode


## Background mode 

To allow the app to record audio in backround, go to your project settings, select your app target, click on "Signing & Capabilities"
click on "+ Capability", select "Background Modes" and enable "Audio, AirPlay, and Picture in Picture"


## Usage

### Initialization

For example, inside a `ViewController` :

````swift  
override func viewDidLoad() {
    ... 
   let stimshopSDk = try! StimShop.Builder().debugEnabled(debug: false).extraMessage(message: "Demo IOS - sent message when a signal is detected").build()
}
````  

Your should set `debugEnabled` to true or false considering your environment (dev/prod).

The `extraMessage` is used for debuging purpose to identify a detection on the backend.


### Observe stimshop bus events

To receive bus events, add your `ViewController` as an observer.  
When an event is to be sent, the callback code will be executed with a `StimShopEvent` as input parameter.

````swift
  StimshopBus().onStimshopEvent(observer: self) { (event) in
            switch (event) {
            case is StimShopReady:
                  //Called when the detectors have been instantiated and checked
                  //SDK is ready to start the scan of the environment
                break
            case is NoAuthorizedSignals:
                // Signal unauthorized detected
                break
            case is MissingPermissions:
                 //Failed to start the detection caused by missing permissions
                 //Lists the missing permissions with it.permissions, request for it, and call StimShop.companion.get().restart()
                 break
            case is StimShopDetectChirp:
                 //Chirp dectected on a channel
                 break
            case is StimShopDetectCode:
                 // Code detected on a channel
                 break
            case is StimShopDetectNotification:
                 // Code has been linked to a notification
                 // Use this notification data
                 break
            case is WiUSChecksumError:
                // Checksum Error related to WiUS
                break
            case is DetectionStarted:
                 //Detection started
                 break
            case is DetectionStopped:
                // Detection stoped
                break
            default:
                break
            }
````

#### StimShopEvents (KMM)

````kotlin
StimShopReady(val isReady: Boolean)
````

````kotlin
NoAuthorizedSignals(val message: String)
````

````kotlin
MissingPermissions(val permissions: List<String>)
````

````kotlin
StimShopDetectChirp(val channelId: Int)
````

````kotlin
StimShopDetectCode(val code: String, val channelId: Int)
````

````kotlin
StimShopDetectNotification(val notification: StimNotification, val item: StimNotificationItem)
````

````kotlin
WiUSChecksumError(val message: String)
````

````kotlin
StimShopError(val code: Int, val errorMessage: String?)
````

````kotlin
DetectionStarted()
````

````kotlin
DetectionStopped()
````

### Notification related classes (KMM)

````kotlin
class StimNotification {
    lateinit var name: String
        internal set
    lateinit var displayName: String
        internal set
    lateinit var type: StimNotificationType
        internal set
    lateinit var startDate: String
        internal set
    lateinit var endDate: String
        internal set
    lateinit var signal: String
        internal set
    var iconUrl: String? = null
        internal set
    var eventId: String? = null
        internal set
    lateinit var items: List<StimNotificationItem>
        internal set
    var delayBetweenDisplay: Int? = null
        internal set
    }
```` 

````kotlin
class StimNotificationItem {
    lateinit var id: String
        internal set
    lateinit var displayType: String
        internal set
    var imageUrl: String? = null
        internal set
    var imageLink: String? = null
        internal set
    var htmlContent: String? = null
        internal set
    var videoLink: String? = null
        internal set
    var numberOfDisplay by Delegates.notNull<Int>()
        internal set
    var delayBetweenDisplay by Delegates.notNull<Int>()
        internal set
    var probability: Int? = null
        internal set
    var quota: Int? = null
        internal set
    var defaultNotification: Boolean = false
        internal set
    var iconUrl: String? = null
        internal set
}
````

````kotlin
enum class StimNotificationType(val value: String) {
    SIMPLE("simple"), RANDOM("random-draw"), INSTANT_WIN("instant-win"), TREASURE_HUNT("treasure-hunt");
}
````


### Start detection

When `StimShopReady`, you can start the detection and receive informations of the detection as stated in the [previous section](#observe-stimshop-bus-events). 

````swift  
    StimShop.companion.get().startDetection()
````  

### Stop the detection when it is no longer needed

You must stop the detection when it is no longer needed with the `stopDetection` method. 
You should stop it on the `deinit` or `viewWillDisappear` method of your `ViewController`.

````swift  
    deinit{
        StimShop.companion.get().stopDetection()
    }
```` 

### Check if SDK is currently detecting


````swift  
    StimShop.companion.get().isDetecting() -> Bool
```` 

### Restart detection

Used typically after adding missing permissions.

````swift  
    StimShop.companion.get().restart()
```` 
