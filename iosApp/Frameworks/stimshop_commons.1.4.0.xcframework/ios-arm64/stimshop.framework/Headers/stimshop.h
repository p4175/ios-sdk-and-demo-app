#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class StimshopApollo_apiCompiledField, StimshopApollo_apiCustomScalarAdapters, StimshopDetectionData, StimshopAddDetectionMutationCompanion, StimshopAddDetectionMutation, StimshopAddDetectionMutationAddDetection, StimshopAddDetectionMutationData, StimshopAuthenticateUserWithPasswordMutationCompanion, StimshopAuthenticateUserWithPasswordMutation, StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordSuccess, StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordFailure, StimshopAuthenticateUserWithPasswordMutationAuthenticateUserWithPassword, StimshopAuthenticateUserWithPasswordMutationData, StimshopAuthenticateUserWithPasswordMutationItem, StimshopConnectQueryCompanion, StimshopConnectQuery, StimshopConnectQuerySdkConnect, StimshopConnectQueryData, StimshopConnectQueryItem, StimshopConnectQueryLicense, StimshopConnectQueryNotification, StimshopConnectQueryUser, StimshopStimShopEvent, StimshopDetectionStarted, StimshopDetectionStopped, StimshopErrorCode, StimshopMessage<T>, StimshopMissingPermissions, StimshopNoAuthorizedSignals, StimshopStimShopCompanion, StimshopApiConfiguration, StimshopResourcesConfiguration, StimshopStimShop, StimshopStimShopBuilder, StimshopStimShopDetectChirp, StimshopStimShopDetectCode, StimshopStimNotification, StimshopStimNotificationItem, StimshopStimShopDetectNotification, StimshopStimShopError, StimshopStimShopReady, StimshopUpdateQuotaMutationCompanion, StimshopUpdateQuotaMutation, StimshopUpdateQuotaMutationData, StimshopUserPwdConnectQueryCompanion, StimshopUserPwdConnectQuery, StimshopUserPwdConnectQueryUserPwdConnect, StimshopUserPwdConnectQueryData, StimshopWiUSChecksumError, StimshopAddDetectionMutation_ResponseAdapter, StimshopAddDetectionMutation_ResponseAdapterAddDetection, StimshopAddDetectionMutation_ResponseAdapterData, StimshopAddDetectionMutation_VariablesAdapter, StimshopAuthenticateUserWithPasswordMutation_ResponseAdapter, StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterAuthenticateUserWithPassword, StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterData, StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterItem, StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterOnUserAuthenticationWithPasswordFailure, StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterOnUserAuthenticationWithPasswordSuccess, StimshopAuthenticateUserWithPasswordMutation_VariablesAdapter, StimshopConnectQuery_ResponseAdapter, StimshopConnectQuery_ResponseAdapterData, StimshopConnectQuery_ResponseAdapterItem, StimshopConnectQuery_ResponseAdapterLicense, StimshopConnectQuery_ResponseAdapterNotification, StimshopConnectQuery_ResponseAdapterSdkConnect, StimshopConnectQuery_ResponseAdapterUser, StimshopConnectQuery_VariablesAdapter, StimshopUpdateQuotaMutation_ResponseAdapter, StimshopUpdateQuotaMutation_ResponseAdapterData, StimshopUpdateQuotaMutation_VariablesAdapter, StimshopUserPwdConnectQuery_ResponseAdapter, StimshopUserPwdConnectQuery_ResponseAdapterData, StimshopUserPwdConnectQuery_ResponseAdapterUserPwdConnect, StimshopUserPwdConnectQuery_VariablesAdapter, StimshopAddDetectionMutationSelections, StimshopApollo_apiCompiledSelection, StimshopAuthenticateUserWithPasswordMutationSelections, StimshopConnectQuerySelections, StimshopUpdateQuotaMutationSelections, StimshopUserPwdConnectQuerySelections, StimshopCommonAnalyzer, StimshopSignalWrapper, StimshopComplexWrapper, StimshopFrequencyChannel, StimshopKotlinThrowable, StimshopKotlinArray<T>, StimshopKotlinException, StimshopKotlinRuntimeException, StimshopDateTimeCompanion, StimshopApollo_apiCustomScalarType, StimshopApollo_apiOptional<__covariant V>, StimshopGraphQLBooleanCompanion, StimshopGraphQLFloatCompanion, StimshopGraphQLIDCompanion, StimshopGraphQLIntCompanion, StimshopGraphQLStringCompanion, StimshopMutationCompanion, StimshopApollo_apiObjectType, StimshopQueryCompanion, StimshopSdkConnectDataCompanion, StimshopSdkLicenseCompanion, StimshopSdkNotificationCompanion, StimshopSdkNotificationItemCompanion, StimshopSdkUserCompanion, StimshopSignalDetectionCompanion, StimshopUserCompanion, StimshopUserAuthenticationWithPasswordFailureCompanion, StimshopUserAuthenticationWithPasswordResultCompanion, StimshopApollo_apiUnionType, StimshopUserAuthenticationWithPasswordSuccessCompanion, StimshopUserPwdDataCompanion, StimshopDetectionData_InputAdapter, StimshopSignalTransform, StimshopKotlinFloatArray, StimshopMaxValIndexFloat, StimshopKotlinPair<__covariant A, __covariant B>, StimshopKotlinAtomicReference<T>, StimshopCommonConstants, StimshopUcheckInConstants, StimshopWiUsConstants, StimshopKotlinEnumCompanion, StimshopKotlinEnum<E>, StimshopSdkLicenseTypeCompanion, StimshopSdkLicenseType, StimshopApiConfigurationBuilder, StimshopDetectionModeCompanion, StimshopDetectionMode, StimshopSessionCompanion, StimshopStimLicense, StimshopSession, StimshopKotlinx_datetimeInstant, StimshopSignalCompanion, StimshopSignal, StimshopStimLicenseCompanion, StimshopStimNotificationCompanion, StimshopStimNotificationType, StimshopStimNotificationItemCompanion, StimshopStimNotificationTypeCompanion, StimshopDataStore, StimshopSecureprefsSecurePrefs, StimshopLogEvent, StimshopConstantsCompanion, StimshopFrequencyChannelCompanion, StimshopPermissionsUtils, StimshopPlatformUtils, StimshopBus, StimshopStimshopBus, StimshopTSUtils, StimshopApollo_apiExecutableVariables, StimshopApollo_apiCompiledFieldBuilder, StimshopApollo_apiCompiledArgument, StimshopApollo_apiCompiledCondition, StimshopApollo_apiCompiledType, StimshopApollo_apiJsonNumber, StimshopApollo_apiCustomScalarAdaptersKey, StimshopApollo_apiCustomScalarAdaptersBuilder, StimshopApollo_apiAdapterContext, StimshopKotlinIllegalStateException, StimshopKotlinNothing, StimshopApollo_apiJsonReaderToken, StimshopApollo_apiCompiledNamedType, StimshopApollo_apiOptionalCompanion, StimshopApollo_apiInterfaceType, StimshopApollo_apiObjectTypeBuilder, StimshopKotlinFloatIterator, StimshopKotlinx_datetimeInstantCompanion, StimshopApollo_apiAdapterContextBuilder, StimshopApollo_apiInterfaceTypeBuilder, StimshopKotlinx_serialization_coreSerializersModule, StimshopKotlinx_serialization_coreSerialKind, StimshopKotlinByteArray, StimshopOkioByteString, StimshopOkioBuffer, StimshopOkioTimeout, StimshopApollo_apiCustomTypeValue<T>, StimshopApollo_apiDeferredFragmentIdentifier, StimshopKotlinByteIterator, StimshopOkioByteStringCompanion, StimshopOkioBufferUnsafeCursor, StimshopOkioTimeoutCompanion, StimshopApollo_apiCustomTypeValueCompanion;

@protocol StimshopApollo_apiAdapter, StimshopApollo_apiJsonWriter, StimshopApollo_apiExecutable, StimshopApollo_apiOperation, StimshopApollo_apiMutation, StimshopApollo_apiExecutableData, StimshopApollo_apiOperationData, StimshopApollo_apiMutationData, StimshopApollo_apiQuery, StimshopApollo_apiQueryData, StimshopApollo_apiJsonReader, StimshopKotlinComparable, StimshopKotlinx_serialization_coreKSerializer, StimshopApollo_apiUpload, StimshopOkioCloseable, StimshopApollo_apiExecutionContextKey, StimshopApollo_apiExecutionContextElement, StimshopApollo_apiExecutionContext, StimshopKotlinIterator, StimshopKotlinx_serialization_coreEncoder, StimshopKotlinx_serialization_coreSerialDescriptor, StimshopKotlinx_serialization_coreSerializationStrategy, StimshopKotlinx_serialization_coreDecoder, StimshopKotlinx_serialization_coreDeserializationStrategy, StimshopOkioBufferedSink, StimshopApollo_apiCustomTypeAdapter, StimshopKotlinx_serialization_coreCompositeEncoder, StimshopKotlinAnnotation, StimshopKotlinx_serialization_coreCompositeDecoder, StimshopOkioSource, StimshopOkioSink, StimshopKotlinx_serialization_coreSerializersModuleCollector, StimshopKotlinKClass, StimshopOkioBufferedSource, StimshopKotlinKDeclarationContainer, StimshopKotlinKAnnotatedElement, StimshopKotlinKClassifier;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wincompatible-property-type"
#pragma clang diagnostic ignored "-Wnullability"

#pragma push_macro("_Nullable_result")
#if !__has_feature(nullability_nullable_result)
#undef _Nullable_result
#define _Nullable_result _Nullable
#endif

__attribute__((swift_name("KotlinBase")))
@interface StimshopBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end

@interface StimshopBase (StimshopBaseCopying) <NSCopying>
@end

__attribute__((swift_name("KotlinMutableSet")))
@interface StimshopMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end

__attribute__((swift_name("KotlinMutableDictionary")))
@interface StimshopMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end

@interface NSError (NSErrorStimshopKotlinException)
@property (readonly) id _Nullable kotlinException;
@end

__attribute__((swift_name("KotlinNumber")))
@interface StimshopNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end

__attribute__((swift_name("KotlinByte")))
@interface StimshopByte : StimshopNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end

__attribute__((swift_name("KotlinUByte")))
@interface StimshopUByte : StimshopNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end

__attribute__((swift_name("KotlinShort")))
@interface StimshopShort : StimshopNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end

__attribute__((swift_name("KotlinUShort")))
@interface StimshopUShort : StimshopNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end

__attribute__((swift_name("KotlinInt")))
@interface StimshopInt : StimshopNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end

__attribute__((swift_name("KotlinUInt")))
@interface StimshopUInt : StimshopNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end

__attribute__((swift_name("KotlinLong")))
@interface StimshopLong : StimshopNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end

__attribute__((swift_name("KotlinULong")))
@interface StimshopULong : StimshopNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end

__attribute__((swift_name("KotlinFloat")))
@interface StimshopFloat : StimshopNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end

__attribute__((swift_name("KotlinDouble")))
@interface StimshopDouble : StimshopNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end

__attribute__((swift_name("KotlinBoolean")))
@interface StimshopBoolean : StimshopNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end

__attribute__((swift_name("Apollo_apiExecutable")))
@protocol StimshopApollo_apiExecutable
@required
- (id<StimshopApollo_apiAdapter>)adapter __attribute__((swift_name("adapter()")));
- (StimshopApollo_apiCompiledField *)rootField __attribute__((swift_name("rootField()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)serializeVariablesWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("serializeVariables(writer:customScalarAdapters:)")));
@end

__attribute__((swift_name("Apollo_apiOperation")))
@protocol StimshopApollo_apiOperation <StimshopApollo_apiExecutable>
@required
- (NSString *)document __attribute__((swift_name("document()")));
- (NSString *)id __attribute__((swift_name("id()")));
- (NSString *)name __attribute__((swift_name("name()")));
@end

__attribute__((swift_name("Apollo_apiMutation")))
@protocol StimshopApollo_apiMutation <StimshopApollo_apiOperation>
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AddDetectionMutation")))
@interface StimshopAddDetectionMutation : StimshopBase <StimshopApollo_apiMutation>
- (instancetype)initWithToken:(NSString *)token apiKey:(NSString *)apiKey data:(StimshopDetectionData *)data __attribute__((swift_name("init(token:apiKey:data:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) StimshopAddDetectionMutationCompanion *companion __attribute__((swift_name("companion")));
- (id<StimshopApollo_apiAdapter>)adapter __attribute__((swift_name("adapter()")));
- (StimshopAddDetectionMutation *)doCopyToken:(NSString *)token apiKey:(NSString *)apiKey data:(StimshopDetectionData *)data __attribute__((swift_name("doCopy(token:apiKey:data:)")));
- (NSString *)document __attribute__((swift_name("document()")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)id __attribute__((swift_name("id()")));
- (NSString *)name __attribute__((swift_name("name()")));
- (StimshopApollo_apiCompiledField *)rootField __attribute__((swift_name("rootField()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)serializeVariablesWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("serializeVariables(writer:customScalarAdapters:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *apiKey __attribute__((swift_name("apiKey")));
@property (readonly) StimshopDetectionData *data __attribute__((swift_name("data")));
@property (readonly) NSString *token __attribute__((swift_name("token")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AddDetectionMutation.AddDetection")))
@interface StimshopAddDetectionMutationAddDetection : StimshopBase
- (instancetype)initWithId:(NSString *)id __attribute__((swift_name("init(id:)"))) __attribute__((objc_designated_initializer));
- (StimshopAddDetectionMutationAddDetection *)doCopyId:(NSString *)id __attribute__((swift_name("doCopy(id:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AddDetectionMutation.Companion")))
@interface StimshopAddDetectionMutationCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAddDetectionMutationCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) NSString *OPERATION_DOCUMENT __attribute__((swift_name("OPERATION_DOCUMENT")));
@property (readonly) NSString *OPERATION_ID __attribute__((swift_name("OPERATION_ID")));
@property (readonly) NSString *OPERATION_NAME __attribute__((swift_name("OPERATION_NAME")));
@end

__attribute__((swift_name("Apollo_apiExecutableData")))
@protocol StimshopApollo_apiExecutableData
@required
@end

__attribute__((swift_name("Apollo_apiOperationData")))
@protocol StimshopApollo_apiOperationData <StimshopApollo_apiExecutableData>
@required
@end

__attribute__((swift_name("Apollo_apiMutationData")))
@protocol StimshopApollo_apiMutationData <StimshopApollo_apiOperationData>
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AddDetectionMutation.Data")))
@interface StimshopAddDetectionMutationData : StimshopBase <StimshopApollo_apiMutationData>
- (instancetype)initWithAddDetection:(StimshopAddDetectionMutationAddDetection * _Nullable)addDetection __attribute__((swift_name("init(addDetection:)"))) __attribute__((objc_designated_initializer));
- (StimshopAddDetectionMutationData *)doCopyAddDetection:(StimshopAddDetectionMutationAddDetection * _Nullable)addDetection __attribute__((swift_name("doCopy(addDetection:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) StimshopAddDetectionMutationAddDetection * _Nullable addDetection __attribute__((swift_name("addDetection")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation")))
@interface StimshopAuthenticateUserWithPasswordMutation : StimshopBase <StimshopApollo_apiMutation>
- (instancetype)initWithEmail:(NSString *)email password:(NSString *)password __attribute__((swift_name("init(email:password:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) StimshopAuthenticateUserWithPasswordMutationCompanion *companion __attribute__((swift_name("companion")));
- (id<StimshopApollo_apiAdapter>)adapter __attribute__((swift_name("adapter()")));
- (StimshopAuthenticateUserWithPasswordMutation *)doCopyEmail:(NSString *)email password:(NSString *)password __attribute__((swift_name("doCopy(email:password:)")));
- (NSString *)document __attribute__((swift_name("document()")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)id __attribute__((swift_name("id()")));
- (NSString *)name __attribute__((swift_name("name()")));
- (StimshopApollo_apiCompiledField *)rootField __attribute__((swift_name("rootField()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)serializeVariablesWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("serializeVariables(writer:customScalarAdapters:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *email __attribute__((swift_name("email")));
@property (readonly) NSString *password __attribute__((swift_name("password")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation.AuthenticateUserWithPassword")))
@interface StimshopAuthenticateUserWithPasswordMutationAuthenticateUserWithPassword : StimshopBase
- (instancetype)initWith__typename:(NSString *)__typename onUserAuthenticationWithPasswordSuccess:(StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordSuccess * _Nullable)onUserAuthenticationWithPasswordSuccess onUserAuthenticationWithPasswordFailure:(StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordFailure * _Nullable)onUserAuthenticationWithPasswordFailure __attribute__((swift_name("init(__typename:onUserAuthenticationWithPasswordSuccess:onUserAuthenticationWithPasswordFailure:)"))) __attribute__((objc_designated_initializer));
- (StimshopAuthenticateUserWithPasswordMutationAuthenticateUserWithPassword *)doCopy__typename:(NSString *)__typename onUserAuthenticationWithPasswordSuccess:(StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordSuccess * _Nullable)onUserAuthenticationWithPasswordSuccess onUserAuthenticationWithPasswordFailure:(StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordFailure * _Nullable)onUserAuthenticationWithPasswordFailure __attribute__((swift_name("doCopy(__typename:onUserAuthenticationWithPasswordSuccess:onUserAuthenticationWithPasswordFailure:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *__typename __attribute__((swift_name("__typename")));
@property (readonly) StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordFailure * _Nullable onUserAuthenticationWithPasswordFailure __attribute__((swift_name("onUserAuthenticationWithPasswordFailure")));
@property (readonly) StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordSuccess * _Nullable onUserAuthenticationWithPasswordSuccess __attribute__((swift_name("onUserAuthenticationWithPasswordSuccess")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation.Companion")))
@interface StimshopAuthenticateUserWithPasswordMutationCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAuthenticateUserWithPasswordMutationCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) NSString *OPERATION_DOCUMENT __attribute__((swift_name("OPERATION_DOCUMENT")));
@property (readonly) NSString *OPERATION_ID __attribute__((swift_name("OPERATION_ID")));
@property (readonly) NSString *OPERATION_NAME __attribute__((swift_name("OPERATION_NAME")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation.Data")))
@interface StimshopAuthenticateUserWithPasswordMutationData : StimshopBase <StimshopApollo_apiMutationData>
- (instancetype)initWithAuthenticateUserWithPassword:(StimshopAuthenticateUserWithPasswordMutationAuthenticateUserWithPassword * _Nullable)authenticateUserWithPassword __attribute__((swift_name("init(authenticateUserWithPassword:)"))) __attribute__((objc_designated_initializer));
- (StimshopAuthenticateUserWithPasswordMutationData *)doCopyAuthenticateUserWithPassword:(StimshopAuthenticateUserWithPasswordMutationAuthenticateUserWithPassword * _Nullable)authenticateUserWithPassword __attribute__((swift_name("doCopy(authenticateUserWithPassword:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) StimshopAuthenticateUserWithPasswordMutationAuthenticateUserWithPassword * _Nullable authenticateUserWithPassword __attribute__((swift_name("authenticateUserWithPassword")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation.Item")))
@interface StimshopAuthenticateUserWithPasswordMutationItem : StimshopBase
- (instancetype)initWithId:(NSString *)id email:(NSString * _Nullable)email name:(NSString * _Nullable)name __attribute__((swift_name("init(id:email:name:)"))) __attribute__((objc_designated_initializer));
- (StimshopAuthenticateUserWithPasswordMutationItem *)doCopyId:(NSString *)id email:(NSString * _Nullable)email name:(NSString * _Nullable)name __attribute__((swift_name("doCopy(id:email:name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable email __attribute__((swift_name("email")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation.OnUserAuthenticationWithPasswordFailure")))
@interface StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordFailure : StimshopBase
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordFailure *)doCopyMessage:(NSString *)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation.OnUserAuthenticationWithPasswordSuccess")))
@interface StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordSuccess : StimshopBase
- (instancetype)initWithItem:(StimshopAuthenticateUserWithPasswordMutationItem *)item sessionToken:(NSString *)sessionToken __attribute__((swift_name("init(item:sessionToken:)"))) __attribute__((objc_designated_initializer));
- (StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordSuccess *)doCopyItem:(StimshopAuthenticateUserWithPasswordMutationItem *)item sessionToken:(NSString *)sessionToken __attribute__((swift_name("doCopy(item:sessionToken:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) StimshopAuthenticateUserWithPasswordMutationItem *item __attribute__((swift_name("item")));
@property (readonly) NSString *sessionToken __attribute__((swift_name("sessionToken")));
@end

__attribute__((swift_name("Apollo_apiQuery")))
@protocol StimshopApollo_apiQuery <StimshopApollo_apiOperation>
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery")))
@interface StimshopConnectQuery : StimshopBase <StimshopApollo_apiQuery>
- (instancetype)initWithToken:(NSString *)token apiKey:(NSString *)apiKey __attribute__((swift_name("init(token:apiKey:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) StimshopConnectQueryCompanion *companion __attribute__((swift_name("companion")));
- (id<StimshopApollo_apiAdapter>)adapter __attribute__((swift_name("adapter()")));
- (StimshopConnectQuery *)doCopyToken:(NSString *)token apiKey:(NSString *)apiKey __attribute__((swift_name("doCopy(token:apiKey:)")));
- (NSString *)document __attribute__((swift_name("document()")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)id __attribute__((swift_name("id()")));
- (NSString *)name __attribute__((swift_name("name()")));
- (StimshopApollo_apiCompiledField *)rootField __attribute__((swift_name("rootField()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)serializeVariablesWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("serializeVariables(writer:customScalarAdapters:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *apiKey __attribute__((swift_name("apiKey")));
@property (readonly) NSString *token __attribute__((swift_name("token")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery.Companion")))
@interface StimshopConnectQueryCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopConnectQueryCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) NSString *OPERATION_DOCUMENT __attribute__((swift_name("OPERATION_DOCUMENT")));
@property (readonly) NSString *OPERATION_ID __attribute__((swift_name("OPERATION_ID")));
@property (readonly) NSString *OPERATION_NAME __attribute__((swift_name("OPERATION_NAME")));
@end

__attribute__((swift_name("Apollo_apiQueryData")))
@protocol StimshopApollo_apiQueryData <StimshopApollo_apiOperationData>
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery.Data")))
@interface StimshopConnectQueryData : StimshopBase <StimshopApollo_apiQueryData>
- (instancetype)initWithSdkConnect:(StimshopConnectQuerySdkConnect * _Nullable)sdkConnect __attribute__((swift_name("init(sdkConnect:)"))) __attribute__((objc_designated_initializer));
- (StimshopConnectQueryData *)doCopySdkConnect:(StimshopConnectQuerySdkConnect * _Nullable)sdkConnect __attribute__((swift_name("doCopy(sdkConnect:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) StimshopConnectQuerySdkConnect * _Nullable sdkConnect __attribute__((swift_name("sdkConnect")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery.Item")))
@interface StimshopConnectQueryItem : StimshopBase
- (instancetype)initWithId:(NSString *)id displayType:(NSString *)displayType imageUrl:(NSString * _Nullable)imageUrl imageLink:(NSString * _Nullable)imageLink htmlContent:(NSString * _Nullable)htmlContent videoLink:(NSString * _Nullable)videoLink numberOfDisplay:(int32_t)numberOfDisplay delayBetweenDisplay:(int32_t)delayBetweenDisplay probability:(StimshopInt * _Nullable)probability defaultNotification:(StimshopBoolean * _Nullable)defaultNotification quota:(StimshopInt * _Nullable)quota iconUrl:(NSString * _Nullable)iconUrl __attribute__((swift_name("init(id:displayType:imageUrl:imageLink:htmlContent:videoLink:numberOfDisplay:delayBetweenDisplay:probability:defaultNotification:quota:iconUrl:)"))) __attribute__((objc_designated_initializer));
- (StimshopConnectQueryItem *)doCopyId:(NSString *)id displayType:(NSString *)displayType imageUrl:(NSString * _Nullable)imageUrl imageLink:(NSString * _Nullable)imageLink htmlContent:(NSString * _Nullable)htmlContent videoLink:(NSString * _Nullable)videoLink numberOfDisplay:(int32_t)numberOfDisplay delayBetweenDisplay:(int32_t)delayBetweenDisplay probability:(StimshopInt * _Nullable)probability defaultNotification:(StimshopBoolean * _Nullable)defaultNotification quota:(StimshopInt * _Nullable)quota iconUrl:(NSString * _Nullable)iconUrl __attribute__((swift_name("doCopy(id:displayType:imageUrl:imageLink:htmlContent:videoLink:numberOfDisplay:delayBetweenDisplay:probability:defaultNotification:quota:iconUrl:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) StimshopBoolean * _Nullable defaultNotification __attribute__((swift_name("defaultNotification")));
@property (readonly) int32_t delayBetweenDisplay __attribute__((swift_name("delayBetweenDisplay")));
@property (readonly) NSString *displayType __attribute__((swift_name("displayType")));
@property (readonly) NSString * _Nullable htmlContent __attribute__((swift_name("htmlContent")));
@property (readonly) NSString * _Nullable iconUrl __attribute__((swift_name("iconUrl")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable imageLink __attribute__((swift_name("imageLink")));
@property (readonly) NSString * _Nullable imageUrl __attribute__((swift_name("imageUrl")));
@property (readonly) int32_t numberOfDisplay __attribute__((swift_name("numberOfDisplay")));
@property (readonly) StimshopInt * _Nullable probability __attribute__((swift_name("probability")));
@property (readonly) StimshopInt * _Nullable quota __attribute__((swift_name("quota")));
@property (readonly) NSString * _Nullable videoLink __attribute__((swift_name("videoLink")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery.License")))
@interface StimshopConnectQueryLicense : StimshopBase
- (instancetype)initWithType:(NSString *)type expirationDate:(NSString *)expirationDate signal:(NSString * _Nullable)signal __attribute__((swift_name("init(type:expirationDate:signal:)"))) __attribute__((objc_designated_initializer));
- (StimshopConnectQueryLicense *)doCopyType:(NSString *)type expirationDate:(NSString *)expirationDate signal:(NSString * _Nullable)signal __attribute__((swift_name("doCopy(type:expirationDate:signal:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *expirationDate __attribute__((swift_name("expirationDate")));
@property (readonly) NSString * _Nullable signal __attribute__((swift_name("signal")));
@property (readonly) NSString *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery.Notification")))
@interface StimshopConnectQueryNotification : StimshopBase
- (instancetype)initWithName:(NSString *)name displayName:(NSString *)displayName type:(NSString *)type startDate:(NSString *)startDate endDate:(NSString *)endDate signal:(NSString *)signal iconUrl:(NSString * _Nullable)iconUrl eventId:(NSString * _Nullable)eventId delayBetweenDisplay:(int32_t)delayBetweenDisplay items:(NSArray<StimshopConnectQueryItem *> *)items __attribute__((swift_name("init(name:displayName:type:startDate:endDate:signal:iconUrl:eventId:delayBetweenDisplay:items:)"))) __attribute__((objc_designated_initializer));
- (StimshopConnectQueryNotification *)doCopyName:(NSString *)name displayName:(NSString *)displayName type:(NSString *)type startDate:(NSString *)startDate endDate:(NSString *)endDate signal:(NSString *)signal iconUrl:(NSString * _Nullable)iconUrl eventId:(NSString * _Nullable)eventId delayBetweenDisplay:(int32_t)delayBetweenDisplay items:(NSArray<StimshopConnectQueryItem *> *)items __attribute__((swift_name("doCopy(name:displayName:type:startDate:endDate:signal:iconUrl:eventId:delayBetweenDisplay:items:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t delayBetweenDisplay __attribute__((swift_name("delayBetweenDisplay")));
@property (readonly) NSString *displayName __attribute__((swift_name("displayName")));
@property (readonly) NSString *endDate __attribute__((swift_name("endDate")));
@property (readonly) NSString * _Nullable eventId __attribute__((swift_name("eventId")));
@property (readonly) NSString * _Nullable iconUrl __attribute__((swift_name("iconUrl")));
@property (readonly) NSArray<StimshopConnectQueryItem *> *items __attribute__((swift_name("items")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *signal __attribute__((swift_name("signal")));
@property (readonly) NSString *startDate __attribute__((swift_name("startDate")));
@property (readonly) NSString *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery.SdkConnect")))
@interface StimshopConnectQuerySdkConnect : StimshopBase
- (instancetype)initWithUser:(StimshopConnectQueryUser * _Nullable)user licenses:(NSArray<id> * _Nullable)licenses notifications:(NSArray<id> * _Nullable)notifications __attribute__((swift_name("init(user:licenses:notifications:)"))) __attribute__((objc_designated_initializer));
- (StimshopConnectQuerySdkConnect *)doCopyUser:(StimshopConnectQueryUser * _Nullable)user licenses:(NSArray<id> * _Nullable)licenses notifications:(NSArray<id> * _Nullable)notifications __attribute__((swift_name("doCopy(user:licenses:notifications:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSArray<StimshopConnectQueryLicense *> * _Nullable)licensesFilterNotNull __attribute__((swift_name("licensesFilterNotNull()")));
- (NSArray<StimshopConnectQueryNotification *> * _Nullable)notificationsFilterNotNull __attribute__((swift_name("notificationsFilterNotNull()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<id> * _Nullable licenses __attribute__((swift_name("licenses")));
@property (readonly) NSArray<id> * _Nullable notifications __attribute__((swift_name("notifications")));
@property (readonly) StimshopConnectQueryUser * _Nullable user __attribute__((swift_name("user")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery.User")))
@interface StimshopConnectQueryUser : StimshopBase
- (instancetype)initWithName:(NSString *)name email:(NSString *)email canDetectAllSignals:(BOOL)canDetectAllSignals __attribute__((swift_name("init(name:email:canDetectAllSignals:)"))) __attribute__((objc_designated_initializer));
- (StimshopConnectQueryUser *)doCopyName:(NSString *)name email:(NSString *)email canDetectAllSignals:(BOOL)canDetectAllSignals __attribute__((swift_name("doCopy(name:email:canDetectAllSignals:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL canDetectAllSignals __attribute__((swift_name("canDetectAllSignals")));
@property (readonly) NSString *email __attribute__((swift_name("email")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end

__attribute__((swift_name("StimShopEvent")))
@interface StimshopStimShopEvent : StimshopBase
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DetectionStarted")))
@interface StimshopDetectionStarted : StimshopStimShopEvent
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)detectionStarted __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopDetectionStarted *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DetectionStopped")))
@interface StimshopDetectionStopped : StimshopStimShopEvent
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)detectionStopped __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopDetectionStopped *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ErrorCode")))
@interface StimshopErrorCode : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)errorCode __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopErrorCode *shared __attribute__((swift_name("shared")));
@property (readonly) int32_t CANNOT_AUTHENTICATE __attribute__((swift_name("CANNOT_AUTHENTICATE")));
@property (readonly) int32_t NO_AUTHORIZED_SIGNALS __attribute__((swift_name("NO_AUTHORIZED_SIGNALS")));
@property (readonly) int32_t NO_DETECTORS_AVAILABLE __attribute__((swift_name("NO_DETECTORS_AVAILABLE")));
@property (readonly) int32_t REQUIRE_BLUETOOTH_ENABLED __attribute__((swift_name("REQUIRE_BLUETOOTH_ENABLED")));
@property (readonly) int32_t REQUIRE_INTERNET_CONNECTIVITY __attribute__((swift_name("REQUIRE_INTERNET_CONNECTIVITY")));
@property (readonly) int32_t REQUIRE_PERMISSIONS __attribute__((swift_name("REQUIRE_PERMISSIONS")));
@property (readonly) int32_t REQUIRE_PREFIX_FROM_API __attribute__((swift_name("REQUIRE_PREFIX_FROM_API")));
@property (readonly) int32_t UNAUTHORIZED_APPLICATION __attribute__((swift_name("UNAUTHORIZED_APPLICATION")));
@end


/**
 * @note annotations
 *   kotlinx.coroutines.DelicateCoroutinesApi
 *   kotlin.ExperimentalUnsignedTypes
*/
__attribute__((swift_name("InternalSignalRecorder")))
@interface StimshopInternalSignalRecorder : StimshopBase

/**
 * @note This method has protected visibility in Kotlin source and is intended only for use by subclasses.
*/
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)startDetection __attribute__((swift_name("startDetection()")));
- (void)stopDetection __attribute__((swift_name("stopDetection()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Message")))
@interface StimshopMessage<T> : StimshopBase
- (instancetype)initWithKey:(NSString *)key message:(NSString * _Nullable)message __attribute__((swift_name("init(key:message:)"))) __attribute__((objc_designated_initializer));
- (StimshopMessage<T> *)doCopyKey:(NSString *)key message:(NSString * _Nullable)message __attribute__((swift_name("doCopy(key:message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *key __attribute__((swift_name("key")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MissingPermissions")))
@interface StimshopMissingPermissions : StimshopStimShopEvent
- (instancetype)initWithPermissions:(NSArray<NSString *> *)permissions __attribute__((swift_name("init(permissions:)"))) __attribute__((objc_designated_initializer));
- (StimshopMissingPermissions *)doCopyPermissions:(NSArray<NSString *> *)permissions __attribute__((swift_name("doCopy(permissions:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<NSString *> *permissions __attribute__((swift_name("permissions")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NoAuthorizedSignals")))
@interface StimshopNoAuthorizedSignals : StimshopStimShopEvent
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (StimshopNoAuthorizedSignals *)doCopyMessage:(NSString *)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end


/**
 * @note annotations
 *   kotlinx.coroutines.DelicateCoroutinesApi
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimShop")))
@interface StimshopStimShop : StimshopBase
@property (class, readonly, getter=companion) StimshopStimShopCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<NSString *> *)getAuthorizedSignals __attribute__((swift_name("getAuthorizedSignals()")));
- (BOOL)isDetecting __attribute__((swift_name("isDetecting()")));
- (void)restart __attribute__((swift_name("restart()")));
- (void)startDetection __attribute__((swift_name("startDetection()")));
- (void)stopDetection __attribute__((swift_name("stopDetection()")));
@property (readonly) StimshopApiConfiguration * _Nullable apiConfiguration __attribute__((swift_name("apiConfiguration")));
@property (readonly) NSString * _Nullable extraMessage __attribute__((swift_name("extraMessage")));
@property (readonly) BOOL isDebugEnabled __attribute__((swift_name("isDebugEnabled")));
@property BOOL isReady __attribute__((swift_name("isReady")));
@property StimshopResourcesConfiguration *sdkConfiguration __attribute__((swift_name("sdkConfiguration")));
@end

__attribute__((swift_name("StimShop.Builder")))
@interface StimshopStimShopBuilder : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));

/**
 * @note This method converts instances of InvalidConfigurationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopStimShop * _Nullable)buildAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("build()")));
- (StimshopStimShopBuilder *)debugEnabledDebug:(BOOL)debug __attribute__((swift_name("debugEnabled(debug:)")));
- (StimshopStimShopBuilder *)extraMessageMessage:(NSString *)message __attribute__((swift_name("extraMessage(message:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimShop.Companion")))
@interface StimshopStimShopCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopStimShopCompanion *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)authenticateEmail:(NSString *)email password:(NSString *)password completionHandler:(void (^)(StimshopBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("authenticate(email:password:completionHandler:)")));
- (StimshopStimShop *)get __attribute__((swift_name("get()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimShopDetectChirp")))
@interface StimshopStimShopDetectChirp : StimshopStimShopEvent
- (instancetype)initWithChannelId:(int32_t)channelId __attribute__((swift_name("init(channelId:)"))) __attribute__((objc_designated_initializer));
- (StimshopStimShopDetectChirp *)doCopyChannelId:(int32_t)channelId __attribute__((swift_name("doCopy(channelId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t channelId __attribute__((swift_name("channelId")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimShopDetectCode")))
@interface StimshopStimShopDetectCode : StimshopStimShopEvent
- (instancetype)initWithCode:(NSString *)code channelId:(int32_t)channelId __attribute__((swift_name("init(code:channelId:)"))) __attribute__((objc_designated_initializer));
- (StimshopStimShopDetectCode *)doCopyCode:(NSString *)code channelId:(int32_t)channelId __attribute__((swift_name("doCopy(code:channelId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t channelId __attribute__((swift_name("channelId")));
@property (readonly) NSString *code __attribute__((swift_name("code")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimShopDetectNotification")))
@interface StimshopStimShopDetectNotification : StimshopStimShopEvent
- (instancetype)initWithNotification:(StimshopStimNotification *)notification item:(StimshopStimNotificationItem *)item __attribute__((swift_name("init(notification:item:)"))) __attribute__((objc_designated_initializer));
- (StimshopStimShopDetectNotification *)doCopyNotification:(StimshopStimNotification *)notification item:(StimshopStimNotificationItem *)item __attribute__((swift_name("doCopy(notification:item:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) StimshopStimNotificationItem *item __attribute__((swift_name("item")));
@property (readonly) StimshopStimNotification *notification __attribute__((swift_name("notification")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimShopError")))
@interface StimshopStimShopError : StimshopStimShopEvent
- (instancetype)initWithCode:(int32_t)code errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("init(code:errorMessage:)"))) __attribute__((objc_designated_initializer));
- (StimshopStimShopError *)doCopyCode:(int32_t)code errorMessage:(NSString * _Nullable)errorMessage __attribute__((swift_name("doCopy(code:errorMessage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t code __attribute__((swift_name("code")));
@property (readonly) NSString * _Nullable errorMessage __attribute__((swift_name("errorMessage")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimShopReady")))
@interface StimshopStimShopReady : StimshopStimShopEvent
- (instancetype)initWithIsReady:(BOOL)isReady __attribute__((swift_name("init(isReady:)"))) __attribute__((objc_designated_initializer));
- (StimshopStimShopReady *)doCopyIsReady:(BOOL)isReady __attribute__((swift_name("doCopy(isReady:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isReady __attribute__((swift_name("isReady")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateQuotaMutation")))
@interface StimshopUpdateQuotaMutation : StimshopBase <StimshopApollo_apiMutation>
- (instancetype)initWithToken:(NSString *)token apiKey:(NSString *)apiKey notificationItemId:(NSString *)notificationItemId __attribute__((swift_name("init(token:apiKey:notificationItemId:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) StimshopUpdateQuotaMutationCompanion *companion __attribute__((swift_name("companion")));
- (id<StimshopApollo_apiAdapter>)adapter __attribute__((swift_name("adapter()")));
- (StimshopUpdateQuotaMutation *)doCopyToken:(NSString *)token apiKey:(NSString *)apiKey notificationItemId:(NSString *)notificationItemId __attribute__((swift_name("doCopy(token:apiKey:notificationItemId:)")));
- (NSString *)document __attribute__((swift_name("document()")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)id __attribute__((swift_name("id()")));
- (NSString *)name __attribute__((swift_name("name()")));
- (StimshopApollo_apiCompiledField *)rootField __attribute__((swift_name("rootField()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)serializeVariablesWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("serializeVariables(writer:customScalarAdapters:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *apiKey __attribute__((swift_name("apiKey")));
@property (readonly) NSString *notificationItemId __attribute__((swift_name("notificationItemId")));
@property (readonly) NSString *token __attribute__((swift_name("token")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateQuotaMutation.Companion")))
@interface StimshopUpdateQuotaMutationCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUpdateQuotaMutationCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) NSString *OPERATION_DOCUMENT __attribute__((swift_name("OPERATION_DOCUMENT")));
@property (readonly) NSString *OPERATION_ID __attribute__((swift_name("OPERATION_ID")));
@property (readonly) NSString *OPERATION_NAME __attribute__((swift_name("OPERATION_NAME")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateQuotaMutation.Data")))
@interface StimshopUpdateQuotaMutationData : StimshopBase <StimshopApollo_apiMutationData>
- (instancetype)initWithUpdateQuota:(StimshopBoolean * _Nullable)updateQuota __attribute__((swift_name("init(updateQuota:)"))) __attribute__((objc_designated_initializer));
- (StimshopUpdateQuotaMutationData *)doCopyUpdateQuota:(StimshopBoolean * _Nullable)updateQuota __attribute__((swift_name("doCopy(updateQuota:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) StimshopBoolean * _Nullable updateQuota __attribute__((swift_name("updateQuota")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserPwdConnectQuery")))
@interface StimshopUserPwdConnectQuery : StimshopBase <StimshopApollo_apiQuery>
- (instancetype)initWithUserId:(NSString *)userId __attribute__((swift_name("init(userId:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) StimshopUserPwdConnectQueryCompanion *companion __attribute__((swift_name("companion")));
- (id<StimshopApollo_apiAdapter>)adapter __attribute__((swift_name("adapter()")));
- (StimshopUserPwdConnectQuery *)doCopyUserId:(NSString *)userId __attribute__((swift_name("doCopy(userId:)")));
- (NSString *)document __attribute__((swift_name("document()")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)id __attribute__((swift_name("id()")));
- (NSString *)name __attribute__((swift_name("name()")));
- (StimshopApollo_apiCompiledField *)rootField __attribute__((swift_name("rootField()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)serializeVariablesWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("serializeVariables(writer:customScalarAdapters:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *userId __attribute__((swift_name("userId")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserPwdConnectQuery.Companion")))
@interface StimshopUserPwdConnectQueryCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUserPwdConnectQueryCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) NSString *OPERATION_DOCUMENT __attribute__((swift_name("OPERATION_DOCUMENT")));
@property (readonly) NSString *OPERATION_ID __attribute__((swift_name("OPERATION_ID")));
@property (readonly) NSString *OPERATION_NAME __attribute__((swift_name("OPERATION_NAME")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserPwdConnectQuery.Data")))
@interface StimshopUserPwdConnectQueryData : StimshopBase <StimshopApollo_apiQueryData>
- (instancetype)initWithUserPwdConnect:(StimshopUserPwdConnectQueryUserPwdConnect * _Nullable)userPwdConnect __attribute__((swift_name("init(userPwdConnect:)"))) __attribute__((objc_designated_initializer));
- (StimshopUserPwdConnectQueryData *)doCopyUserPwdConnect:(StimshopUserPwdConnectQueryUserPwdConnect * _Nullable)userPwdConnect __attribute__((swift_name("doCopy(userPwdConnect:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) StimshopUserPwdConnectQueryUserPwdConnect * _Nullable userPwdConnect __attribute__((swift_name("userPwdConnect")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserPwdConnectQuery.UserPwdConnect")))
@interface StimshopUserPwdConnectQueryUserPwdConnect : StimshopBase
- (instancetype)initWithSignals:(NSArray<id> * _Nullable)signals canDetectAllSignals:(StimshopBoolean * _Nullable)canDetectAllSignals token:(NSString * _Nullable)token __attribute__((swift_name("init(signals:canDetectAllSignals:token:)"))) __attribute__((objc_designated_initializer));
- (StimshopUserPwdConnectQueryUserPwdConnect *)doCopySignals:(NSArray<id> * _Nullable)signals canDetectAllSignals:(StimshopBoolean * _Nullable)canDetectAllSignals token:(NSString * _Nullable)token __attribute__((swift_name("doCopy(signals:canDetectAllSignals:token:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSArray<NSString *> * _Nullable)signalsFilterNotNull __attribute__((swift_name("signalsFilterNotNull()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) StimshopBoolean * _Nullable canDetectAllSignals __attribute__((swift_name("canDetectAllSignals")));
@property (readonly) NSArray<id> * _Nullable signals __attribute__((swift_name("signals")));
@property (readonly) NSString * _Nullable token __attribute__((swift_name("token")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WiUSChecksumError")))
@interface StimshopWiUSChecksumError : StimshopStimShopEvent
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (StimshopWiUSChecksumError *)doCopyMessage:(NSString *)message __attribute__((swift_name("doCopy(message:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AddDetectionMutation_ResponseAdapter")))
@interface StimshopAddDetectionMutation_ResponseAdapter : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)addDetectionMutation_ResponseAdapter __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAddDetectionMutation_ResponseAdapter *shared __attribute__((swift_name("shared")));
@end

__attribute__((swift_name("Apollo_apiAdapter")))
@protocol StimshopApollo_apiAdapter
@required

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(id _Nullable)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AddDetectionMutation_ResponseAdapter.AddDetection")))
@interface StimshopAddDetectionMutation_ResponseAdapterAddDetection : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)addDetection __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAddDetectionMutation_ResponseAdapterAddDetection *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopAddDetectionMutationAddDetection * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopAddDetectionMutationAddDetection *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AddDetectionMutation_ResponseAdapter.Data")))
@interface StimshopAddDetectionMutation_ResponseAdapterData : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)data __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAddDetectionMutation_ResponseAdapterData *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopAddDetectionMutationData * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopAddDetectionMutationData *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AddDetectionMutation_VariablesAdapter")))
@interface StimshopAddDetectionMutation_VariablesAdapter : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)addDetectionMutation_VariablesAdapter __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAddDetectionMutation_VariablesAdapter *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopAddDetectionMutation * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopAddDetectionMutation *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation_ResponseAdapter")))
@interface StimshopAuthenticateUserWithPasswordMutation_ResponseAdapter : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)authenticateUserWithPasswordMutation_ResponseAdapter __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAuthenticateUserWithPasswordMutation_ResponseAdapter *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation_ResponseAdapter.AuthenticateUserWithPassword")))
@interface StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterAuthenticateUserWithPassword : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)authenticateUserWithPassword __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterAuthenticateUserWithPassword *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopAuthenticateUserWithPasswordMutationAuthenticateUserWithPassword * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopAuthenticateUserWithPasswordMutationAuthenticateUserWithPassword *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation_ResponseAdapter.Data")))
@interface StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterData : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)data __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterData *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopAuthenticateUserWithPasswordMutationData * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopAuthenticateUserWithPasswordMutationData *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation_ResponseAdapter.Item")))
@interface StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterItem : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)item __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterItem *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopAuthenticateUserWithPasswordMutationItem * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopAuthenticateUserWithPasswordMutationItem *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation_ResponseAdapter.OnUserAuthenticationWithPasswordFailure")))
@interface StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterOnUserAuthenticationWithPasswordFailure : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)onUserAuthenticationWithPasswordFailure __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterOnUserAuthenticationWithPasswordFailure *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordFailure * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordFailure *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation_ResponseAdapter.OnUserAuthenticationWithPasswordSuccess")))
@interface StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterOnUserAuthenticationWithPasswordSuccess : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)onUserAuthenticationWithPasswordSuccess __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAuthenticateUserWithPasswordMutation_ResponseAdapterOnUserAuthenticationWithPasswordSuccess *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordSuccess * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopAuthenticateUserWithPasswordMutationOnUserAuthenticationWithPasswordSuccess *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutation_VariablesAdapter")))
@interface StimshopAuthenticateUserWithPasswordMutation_VariablesAdapter : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)authenticateUserWithPasswordMutation_VariablesAdapter __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAuthenticateUserWithPasswordMutation_VariablesAdapter *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopAuthenticateUserWithPasswordMutation * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopAuthenticateUserWithPasswordMutation *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery_ResponseAdapter")))
@interface StimshopConnectQuery_ResponseAdapter : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)connectQuery_ResponseAdapter __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopConnectQuery_ResponseAdapter *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery_ResponseAdapter.Data")))
@interface StimshopConnectQuery_ResponseAdapterData : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)data __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopConnectQuery_ResponseAdapterData *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopConnectQueryData * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopConnectQueryData *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery_ResponseAdapter.Item")))
@interface StimshopConnectQuery_ResponseAdapterItem : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)item __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopConnectQuery_ResponseAdapterItem *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopConnectQueryItem * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopConnectQueryItem *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery_ResponseAdapter.License")))
@interface StimshopConnectQuery_ResponseAdapterLicense : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)license __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopConnectQuery_ResponseAdapterLicense *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopConnectQueryLicense * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopConnectQueryLicense *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery_ResponseAdapter.Notification")))
@interface StimshopConnectQuery_ResponseAdapterNotification : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)notification __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopConnectQuery_ResponseAdapterNotification *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopConnectQueryNotification * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopConnectQueryNotification *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery_ResponseAdapter.SdkConnect")))
@interface StimshopConnectQuery_ResponseAdapterSdkConnect : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)sdkConnect __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopConnectQuery_ResponseAdapterSdkConnect *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopConnectQuerySdkConnect * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopConnectQuerySdkConnect *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery_ResponseAdapter.User")))
@interface StimshopConnectQuery_ResponseAdapterUser : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)user __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopConnectQuery_ResponseAdapterUser *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopConnectQueryUser * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopConnectQueryUser *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuery_VariablesAdapter")))
@interface StimshopConnectQuery_VariablesAdapter : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)connectQuery_VariablesAdapter __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopConnectQuery_VariablesAdapter *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopConnectQuery * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopConnectQuery *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateQuotaMutation_ResponseAdapter")))
@interface StimshopUpdateQuotaMutation_ResponseAdapter : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)updateQuotaMutation_ResponseAdapter __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUpdateQuotaMutation_ResponseAdapter *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateQuotaMutation_ResponseAdapter.Data")))
@interface StimshopUpdateQuotaMutation_ResponseAdapterData : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)data __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUpdateQuotaMutation_ResponseAdapterData *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopUpdateQuotaMutationData * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopUpdateQuotaMutationData *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateQuotaMutation_VariablesAdapter")))
@interface StimshopUpdateQuotaMutation_VariablesAdapter : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)updateQuotaMutation_VariablesAdapter __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUpdateQuotaMutation_VariablesAdapter *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopUpdateQuotaMutation * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopUpdateQuotaMutation *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserPwdConnectQuery_ResponseAdapter")))
@interface StimshopUserPwdConnectQuery_ResponseAdapter : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)userPwdConnectQuery_ResponseAdapter __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUserPwdConnectQuery_ResponseAdapter *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserPwdConnectQuery_ResponseAdapter.Data")))
@interface StimshopUserPwdConnectQuery_ResponseAdapterData : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)data __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUserPwdConnectQuery_ResponseAdapterData *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopUserPwdConnectQueryData * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopUserPwdConnectQueryData *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserPwdConnectQuery_ResponseAdapter.UserPwdConnect")))
@interface StimshopUserPwdConnectQuery_ResponseAdapterUserPwdConnect : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)userPwdConnect __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUserPwdConnectQuery_ResponseAdapterUserPwdConnect *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopUserPwdConnectQueryUserPwdConnect * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopUserPwdConnectQueryUserPwdConnect *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@property (readonly) NSArray<NSString *> *RESPONSE_NAMES __attribute__((swift_name("RESPONSE_NAMES")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserPwdConnectQuery_VariablesAdapter")))
@interface StimshopUserPwdConnectQuery_VariablesAdapter : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)userPwdConnectQuery_VariablesAdapter __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUserPwdConnectQuery_VariablesAdapter *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopUserPwdConnectQuery * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopUserPwdConnectQuery *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AddDetectionMutationSelections")))
@interface StimshopAddDetectionMutationSelections : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)addDetectionMutationSelections __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAddDetectionMutationSelections *shared __attribute__((swift_name("shared")));
@property (readonly) NSArray<StimshopApollo_apiCompiledSelection *> *__root __attribute__((swift_name("__root")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthenticateUserWithPasswordMutationSelections")))
@interface StimshopAuthenticateUserWithPasswordMutationSelections : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)authenticateUserWithPasswordMutationSelections __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopAuthenticateUserWithPasswordMutationSelections *shared __attribute__((swift_name("shared")));
@property (readonly) NSArray<StimshopApollo_apiCompiledSelection *> *__root __attribute__((swift_name("__root")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectQuerySelections")))
@interface StimshopConnectQuerySelections : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)connectQuerySelections __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopConnectQuerySelections *shared __attribute__((swift_name("shared")));
@property (readonly) NSArray<StimshopApollo_apiCompiledSelection *> *__root __attribute__((swift_name("__root")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdateQuotaMutationSelections")))
@interface StimshopUpdateQuotaMutationSelections : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)updateQuotaMutationSelections __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUpdateQuotaMutationSelections *shared __attribute__((swift_name("shared")));
@property (readonly) NSArray<StimshopApollo_apiCompiledSelection *> *__root __attribute__((swift_name("__root")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserPwdConnectQuerySelections")))
@interface StimshopUserPwdConnectQuerySelections : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)userPwdConnectQuerySelections __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUserPwdConnectQuerySelections *shared __attribute__((swift_name("shared")));
@property (readonly) NSArray<StimshopApollo_apiCompiledSelection *> *__root __attribute__((swift_name("__root")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CommonAnalyzer")))
@interface StimshopCommonAnalyzer : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)commonAnalyzer __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopCommonAnalyzer *shared __attribute__((swift_name("shared")));
- (void)startAnalysisSignal:(StimshopSignalWrapper *)signal index:(int32_t)index compareDataUp:(StimshopComplexWrapper *)compareDataUp compareDataDown:(StimshopComplexWrapper *)compareDataDown channel:(StimshopFrequencyChannel *)channel frameCount:(StimshopInt * _Nullable)frameCount __attribute__((swift_name("startAnalysis(signal:index:compareDataUp:compareDataDown:channel:frameCount:)")));
- (void)startAnalysisWiUsSignal:(StimshopSignalWrapper *)signal index:(int32_t)index __attribute__((swift_name("startAnalysisWiUs(signal:index:)")));
@end

__attribute__((swift_name("KotlinThrowable")))
@interface StimshopKotlinThrowable : StimshopBase
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));

/**
 * @note annotations
 *   kotlin.experimental.ExperimentalNativeApi
*/
- (StimshopKotlinArray<NSString *> *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) StimshopKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
- (NSError *)asError __attribute__((swift_name("asError()")));
@end

__attribute__((swift_name("KotlinException")))
@interface StimshopKotlinException : StimshopKotlinThrowable
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((swift_name("KotlinRuntimeException")))
@interface StimshopKotlinRuntimeException : StimshopKotlinException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ChannelNotFoundException")))
@interface StimshopChannelNotFoundException : StimshopKotlinRuntimeException
- (instancetype)initWithChannelId:(int32_t)channelId __attribute__((swift_name("init(channelId:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithCause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InvalidConfigurationException")))
@interface StimshopInvalidConfigurationException : StimshopKotlinRuntimeException
- (instancetype)initWithMessage:(NSString *)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithCause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DateTime")))
@interface StimshopDateTime : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopDateTimeCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DateTime.Companion")))
@interface StimshopDateTimeCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopDateTimeCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiCustomScalarType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DetectionData")))
@interface StimshopDetectionData : StimshopBase
- (instancetype)initWithSignal:(NSString *)signal date:(NSString *)date os:(NSString *)os version:(NSString *)version model:(NSString *)model uuid:(NSString *)uuid chanel:(int32_t)chanel extra:(StimshopApollo_apiOptional<NSString *> *)extra __attribute__((swift_name("init(signal:date:os:version:model:uuid:chanel:extra:)"))) __attribute__((objc_designated_initializer));
- (StimshopDetectionData *)doCopySignal:(NSString *)signal date:(NSString *)date os:(NSString *)os version:(NSString *)version model:(NSString *)model uuid:(NSString *)uuid chanel:(int32_t)chanel extra:(StimshopApollo_apiOptional<NSString *> *)extra __attribute__((swift_name("doCopy(signal:date:os:version:model:uuid:chanel:extra:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t chanel __attribute__((swift_name("chanel")));
@property (readonly) NSString *date __attribute__((swift_name("date")));
@property (readonly) StimshopApollo_apiOptional<NSString *> *extra __attribute__((swift_name("extra")));
@property (readonly) NSString *model __attribute__((swift_name("model")));
@property (readonly) NSString *os __attribute__((swift_name("os")));
@property (readonly) NSString *signal __attribute__((swift_name("signal")));
@property (readonly) NSString *uuid __attribute__((swift_name("uuid")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLBoolean")))
@interface StimshopGraphQLBoolean : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopGraphQLBooleanCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLBoolean.Companion")))
@interface StimshopGraphQLBooleanCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopGraphQLBooleanCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiCustomScalarType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLFloat")))
@interface StimshopGraphQLFloat : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopGraphQLFloatCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLFloat.Companion")))
@interface StimshopGraphQLFloatCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopGraphQLFloatCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiCustomScalarType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLID")))
@interface StimshopGraphQLID : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopGraphQLIDCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLID.Companion")))
@interface StimshopGraphQLIDCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopGraphQLIDCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiCustomScalarType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLInt")))
@interface StimshopGraphQLInt : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopGraphQLIntCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLInt.Companion")))
@interface StimshopGraphQLIntCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopGraphQLIntCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiCustomScalarType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLString")))
@interface StimshopGraphQLString : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopGraphQLStringCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GraphQLString.Companion")))
@interface StimshopGraphQLStringCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopGraphQLStringCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiCustomScalarType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Mutation")))
@interface StimshopMutation : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopMutationCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Mutation.Companion")))
@interface StimshopMutationCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopMutationCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiObjectType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Query")))
@interface StimshopQuery : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopQueryCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Query.Companion")))
@interface StimshopQueryCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopQueryCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiObjectType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SdkConnectData")))
@interface StimshopSdkConnectData : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopSdkConnectDataCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SdkConnectData.Companion")))
@interface StimshopSdkConnectDataCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopSdkConnectDataCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiObjectType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SdkLicense")))
@interface StimshopSdkLicense : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopSdkLicenseCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SdkLicense.Companion")))
@interface StimshopSdkLicenseCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopSdkLicenseCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiObjectType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SdkNotification")))
@interface StimshopSdkNotification : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopSdkNotificationCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SdkNotification.Companion")))
@interface StimshopSdkNotificationCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopSdkNotificationCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiObjectType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SdkNotificationItem")))
@interface StimshopSdkNotificationItem : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopSdkNotificationItemCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SdkNotificationItem.Companion")))
@interface StimshopSdkNotificationItemCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopSdkNotificationItemCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiObjectType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SdkUser")))
@interface StimshopSdkUser : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopSdkUserCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SdkUser.Companion")))
@interface StimshopSdkUserCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopSdkUserCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiObjectType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SignalDetection")))
@interface StimshopSignalDetection : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopSignalDetectionCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SignalDetection.Companion")))
@interface StimshopSignalDetectionCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopSignalDetectionCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiObjectType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("User")))
@interface StimshopUser : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopUserCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("User.Companion")))
@interface StimshopUserCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUserCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiObjectType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserAuthenticationWithPasswordFailure")))
@interface StimshopUserAuthenticationWithPasswordFailure : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopUserAuthenticationWithPasswordFailureCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserAuthenticationWithPasswordFailure.Companion")))
@interface StimshopUserAuthenticationWithPasswordFailureCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUserAuthenticationWithPasswordFailureCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiObjectType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserAuthenticationWithPasswordResult")))
@interface StimshopUserAuthenticationWithPasswordResult : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopUserAuthenticationWithPasswordResultCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserAuthenticationWithPasswordResult.Companion")))
@interface StimshopUserAuthenticationWithPasswordResultCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUserAuthenticationWithPasswordResultCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiUnionType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserAuthenticationWithPasswordSuccess")))
@interface StimshopUserAuthenticationWithPasswordSuccess : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopUserAuthenticationWithPasswordSuccessCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserAuthenticationWithPasswordSuccess.Companion")))
@interface StimshopUserAuthenticationWithPasswordSuccessCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUserAuthenticationWithPasswordSuccessCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiObjectType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserPwdData")))
@interface StimshopUserPwdData : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopUserPwdDataCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserPwdData.Companion")))
@interface StimshopUserPwdDataCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUserPwdDataCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiObjectType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DetectionData_InputAdapter")))
@interface StimshopDetectionData_InputAdapter : StimshopBase <StimshopApollo_apiAdapter>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)detectionData_InputAdapter __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopDetectionData_InputAdapter *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopDetectionData * _Nullable)fromJsonReader:(id<StimshopApollo_apiJsonReader>)reader customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("fromJson(reader:customScalarAdapters:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)toJsonWriter:(id<StimshopApollo_apiJsonWriter>)writer customScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters value:(StimshopDetectionData *)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("toJson(writer:customScalarAdapters:value:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ComplexWrapper")))
@interface StimshopComplexWrapper : StimshopBase
- (instancetype)initWithInput:(void *)input __attribute__((swift_name("init(input:)"))) __attribute__((objc_designated_initializer));
@property void *native __attribute__((swift_name("native")));
@end


/**
 * @note annotations
 *   kotlin.ExperimentalUnsignedTypes
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SignalTransform")))
@interface StimshopSignalTransform : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)signalTransform __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopSignalTransform *shared __attribute__((swift_name("shared")));
- (StimshopKotlinFloatArray *)computeEnergySignalToDecode:(StimshopSignalWrapper *)signalToDecode start:(int32_t)start size:(int32_t)size __attribute__((swift_name("computeEnergy(signalToDecode:start:size:)")));
- (StimshopKotlinPair<StimshopMaxValIndexFloat *, StimshopMaxValIndexFloat *> *)detectChirpWithAllSigWrapper:(StimshopSignalWrapper *)wrapper chirpUp:(StimshopComplexWrapper *)chirpUp chirpDown:(StimshopComplexWrapper *)chirpDown __attribute__((swift_name("detectChirpWithAllSig(wrapper:chirpUp:chirpDown:)")));
- (StimshopKotlinPair<StimshopMaxValIndexFloat *, StimshopMaxValIndexFloat *> *)detectChirpWithAllSigWrapper:(StimshopSignalWrapper *)wrapper chirpUp:(StimshopComplexWrapper *)chirpUp chirpDown:(StimshopComplexWrapper *)chirpDown frameCount:(int32_t)frameCount __attribute__((swift_name("detectChirpWithAllSig(wrapper:chirpUp:chirpDown:frameCount:)")));
- (StimshopKotlinPair<StimshopMaxValIndexFloat *, StimshopMaxValIndexFloat *> *)findMaxIndexesSignalToDecode:(StimshopSignalWrapper *)signalToDecode from:(int32_t)from to:(int32_t)to compareDataUp:(StimshopComplexWrapper *)compareDataUp compareDataDown:(StimshopComplexWrapper *)compareDataDown __attribute__((swift_name("findMaxIndexes(signalToDecode:from:to:compareDataUp:compareDataDown:)")));
- (StimshopComplexWrapper *)generateCompareChirp:(StimshopKotlinFloatArray *)chirp __attribute__((swift_name("generateCompare(chirp:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SignalWrapper")))
@interface StimshopSignalWrapper : StimshopBase
- (instancetype)initWithSignalInput:(void *)signalInput sizeC:(int32_t)sizeC __attribute__((swift_name("init(signalInput:sizeC:)"))) __attribute__((objc_designated_initializer));
- (void)dispose __attribute__((swift_name("dispose()")));
@property StimshopKotlinAtomicReference<id> *signalCopy __attribute__((swift_name("signalCopy")));
@property int32_t size __attribute__((swift_name("size")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CommonConstants")))
@interface StimshopCommonConstants : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)commonConstants __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopCommonConstants *shared __attribute__((swift_name("shared")));
@property (readonly) int32_t FE __attribute__((swift_name("FE")));
@property (readonly) int32_t T __attribute__((swift_name("T")));
@property (readonly) int32_t η __attribute__((swift_name("η")));
@property (readonly) int32_t σ __attribute__((swift_name("σ")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UcheckInConstants")))
@interface StimshopUcheckInConstants : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)ucheckInConstants __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopUcheckInConstants *shared __attribute__((swift_name("shared")));
@property (readonly) int32_t B_UCHECK __attribute__((swift_name("B_UCHECK")));
@property (readonly) int32_t ECART_DETECT __attribute__((swift_name("ECART_DETECT")));
@property (readonly) NSArray<StimshopInt *> *HAMMING_IDX __attribute__((swift_name("HAMMING_IDX")));
@property (readonly) StimshopKotlinArray<StimshopInt *> *LEFT_TABLE __attribute__((swift_name("LEFT_TABLE")));
@property (readonly) int32_t LENGTH_SIGNAL __attribute__((swift_name("LENGTH_SIGNAL")));
@property (readonly) int32_t MARGE_DETECT __attribute__((swift_name("MARGE_DETECT")));
@property (readonly) int32_t NB_BITS __attribute__((swift_name("NB_BITS")));
@property (readonly) int32_t NB_BITS_HAMMING __attribute__((swift_name("NB_BITS_HAMMING")));
@property (readonly) int32_t NB_BITS_PER_SYMB __attribute__((swift_name("NB_BITS_PER_SYMB")));
@property (readonly) StimshopKotlinArray<StimshopInt *> *RIGHT_TABLE __attribute__((swift_name("RIGHT_TABLE")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WiUsConstants")))
@interface StimshopWiUsConstants : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)wiUsConstants __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopWiUsConstants *shared __attribute__((swift_name("shared")));
@property (readonly) int32_t B_WIUS __attribute__((swift_name("B_WIUS")));
@property (readonly) StimshopMutableDictionary<StimshopKotlinPair<StimshopInt *, StimshopInt *> *, NSString *> *DICT __attribute__((swift_name("DICT")));
@property (readonly) NSMutableArray<StimshopInt *> *ID __attribute__((swift_name("ID")));
@property (readonly) int32_t MARGE_DETECTION_BETWEEN_TWO_F __attribute__((swift_name("MARGE_DETECTION_BETWEEN_TWO_F")));
@property (readonly) int32_t NF __attribute__((swift_name("NF")));
@property (readonly) int32_t TB __attribute__((swift_name("TB")));
@end

__attribute__((swift_name("KotlinComparable")))
@protocol StimshopKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end

__attribute__((swift_name("KotlinEnum")))
@interface StimshopKotlinEnum<E> : StimshopBase <StimshopKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) StimshopKotlinEnumCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(E)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly, getter=name_) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SdkLicenseType")))
@interface StimshopSdkLicenseType : StimshopKotlinEnum<StimshopSdkLicenseType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) StimshopSdkLicenseTypeCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) StimshopSdkLicenseType *ucheckin __attribute__((swift_name("ucheckin")));
@property (class, readonly) StimshopSdkLicenseType *wius __attribute__((swift_name("wius")));
@property (class, readonly) StimshopSdkLicenseType *none __attribute__((swift_name("none")));
+ (StimshopKotlinArray<StimshopSdkLicenseType *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<StimshopSdkLicenseType *> *entries __attribute__((swift_name("entries")));
@property (readonly, getter=id_) int32_t id __attribute__((swift_name("id")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SdkLicenseType.Companion")))
@interface StimshopSdkLicenseTypeCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopSdkLicenseTypeCompanion *shared __attribute__((swift_name("shared")));
- (StimshopSdkLicenseType *)valueOfId:(int32_t)id __attribute__((swift_name("valueOf(id:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiConfiguration")))
@interface StimshopApiConfiguration : StimshopBase
- (id)getAuthorizedSignals __attribute__((swift_name("getAuthorizedSignals()")));
- (NSArray<StimshopStimNotification *> *)getNotifications __attribute__((swift_name("getNotifications()")));
- (NSString *)getPrefix __attribute__((swift_name("getPrefix()")));
- (NSArray<StimshopSdkLicenseType *> *)getSdkLicenseType __attribute__((swift_name("getSdkLicenseType()")));
- (BOOL)isAdmin __attribute__((swift_name("isAdmin()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiConfiguration.Builder")))
@interface StimshopApiConfigurationBuilder : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (StimshopApiConfigurationBuilder *)authorizedSdkLicenseTypeSdkLicenseType:(NSArray<StimshopSdkLicenseType *> *)sdkLicenseType __attribute__((swift_name("authorizedSdkLicenseType(sdkLicenseType:)")));
- (StimshopApiConfigurationBuilder *)authorizedSignalsAuthorizedSignals:(NSArray<NSString *> *)authorizedSignals __attribute__((swift_name("authorizedSignals(authorizedSignals:)")));

/**
 * @note This method converts instances of InvalidConfigurationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopApiConfiguration * _Nullable)buildAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("build()")));
- (StimshopApiConfigurationBuilder *)isAdminIsAdmin:(BOOL)isAdmin __attribute__((swift_name("isAdmin(isAdmin:)")));
- (StimshopApiConfigurationBuilder *)notificationsNotifications:(NSArray<StimshopStimNotification *> *)notifications __attribute__((swift_name("notifications(notifications:)")));
- (StimshopApiConfigurationBuilder *)prefixPrefix:(NSString *)prefix __attribute__((swift_name("prefix(prefix:)")));
@property StimshopApiConfiguration *configuration __attribute__((swift_name("configuration")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DetectionMode")))
@interface StimshopDetectionMode : StimshopKotlinEnum<StimshopDetectionMode *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) StimshopDetectionModeCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) StimshopDetectionMode *ucheckin __attribute__((swift_name("ucheckin")));
@property (class, readonly) StimshopDetectionMode *wius __attribute__((swift_name("wius")));
+ (StimshopKotlinArray<StimshopDetectionMode *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<StimshopDetectionMode *> *entries __attribute__((swift_name("entries")));
@property (readonly) int32_t type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DetectionMode.Companion")))
@interface StimshopDetectionModeCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopDetectionModeCompanion *shared __attribute__((swift_name("shared")));
- (StimshopDetectionMode *)fromTypeType:(int32_t)type __attribute__((swift_name("fromType(type:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResourcesConfiguration")))
@interface StimshopResourcesConfiguration : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)resourcesConfiguration __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopResourcesConfiguration *shared __attribute__((swift_name("shared")));

/**
 * @note This method converts instances of InvalidConfigurationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)initializeConfigurationAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("initializeConfiguration()")));
@property NSArray<StimshopFrequencyChannel *> *activesChannels __attribute__((swift_name("activesChannels")));
@property NSString *apiKey __attribute__((swift_name("apiKey")));
@property StimshopDetectionMode * _Nullable detectionMode __attribute__((swift_name("detectionMode")));
@property NSString *token __attribute__((swift_name("token")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Session")))
@interface StimshopSession : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopSessionCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<NSString *> *)getSignals __attribute__((swift_name("getSignals()")));
- (BOOL)hasLicenseValid __attribute__((swift_name("hasLicenseValid()")));
- (NSString *)serializable __attribute__((swift_name("serializable()")));
@property NSString * _Nullable error __attribute__((swift_name("error")));
@property NSString * _Nullable id __attribute__((swift_name("id")));
@property BOOL isAdmin __attribute__((swift_name("isAdmin")));
@property NSString * _Nullable name __attribute__((swift_name("name")));
@property NSArray<NSString *> *signalsWithoutLicense __attribute__((swift_name("signalsWithoutLicense")));
@property NSMutableArray<StimshopStimLicense *> *stimLicences __attribute__((swift_name("stimLicences")));
@property NSMutableArray<StimshopStimNotification *> *stimNotifications __attribute__((swift_name("stimNotifications")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Session.Companion")))
@interface StimshopSessionCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopSessionCompanion *shared __attribute__((swift_name("shared")));
- (StimshopSession *)createUser:(StimshopConnectQueryUser * _Nullable)user licenses:(NSArray<StimshopConnectQueryLicense *> *)licenses notifications:(NSArray<StimshopConnectQueryNotification *> *)notifications __attribute__((swift_name("create(user:licenses:notifications:)")));
- (StimshopSession * _Nullable)fromStringSessionStr:(NSString * _Nullable)sessionStr __attribute__((swift_name("fromString(sessionStr:)")));
- (id<StimshopKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Signal")))
@interface StimshopSignal : StimshopBase
- (instancetype)initWithId:(NSString * _Nullable)id code:(NSString *)code state:(NSString * _Nullable)state created:(StimshopKotlinx_datetimeInstant * _Nullable)created __attribute__((swift_name("init(id:code:state:created:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) StimshopSignalCompanion *companion __attribute__((swift_name("companion")));
- (StimshopSignal *)doCopyId:(NSString * _Nullable)id code:(NSString *)code state:(NSString * _Nullable)state created:(StimshopKotlinx_datetimeInstant * _Nullable)created __attribute__((swift_name("doCopy(id:code:state:created:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property NSString *code __attribute__((swift_name("code")));
@property StimshopKotlinx_datetimeInstant * _Nullable created __attribute__((swift_name("created")));
@property (readonly) NSString * _Nullable id __attribute__((swift_name("id")));
@property NSString * _Nullable state __attribute__((swift_name("state")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Signal.Companion")))
@interface StimshopSignalCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopSignalCompanion *shared __attribute__((swift_name("shared")));
- (id<StimshopKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimLicense")))
@interface StimshopStimLicense : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopStimLicenseCompanion *companion __attribute__((swift_name("companion")));
@property StimshopKotlinx_datetimeInstant *expirationDate __attribute__((swift_name("expirationDate")));
@property NSString * _Nullable signal __attribute__((swift_name("signal")));
@property NSString *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimLicense.Companion")))
@interface StimshopStimLicenseCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopStimLicenseCompanion *shared __attribute__((swift_name("shared")));
- (StimshopStimLicense *)createLicense:(StimshopConnectQueryLicense *)license __attribute__((swift_name("create(license:)")));
- (id<StimshopKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimNotification")))
@interface StimshopStimNotification : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopStimNotificationCompanion *companion __attribute__((swift_name("companion")));
@property (readonly) StimshopInt * _Nullable delayBetweenDisplay __attribute__((swift_name("delayBetweenDisplay")));
@property (readonly) NSString *displayName __attribute__((swift_name("displayName")));
@property (readonly) NSString *endDate __attribute__((swift_name("endDate")));
@property (readonly) NSString * _Nullable eventId __attribute__((swift_name("eventId")));
@property (readonly) NSString * _Nullable iconUrl __attribute__((swift_name("iconUrl")));
@property (readonly) NSArray<StimshopStimNotificationItem *> *items __attribute__((swift_name("items")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *signal __attribute__((swift_name("signal")));
@property (readonly) NSString *startDate __attribute__((swift_name("startDate")));
@property (readonly) StimshopStimNotificationType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimNotification.Companion")))
@interface StimshopStimNotificationCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopStimNotificationCompanion *shared __attribute__((swift_name("shared")));
- (StimshopStimNotification *)createNotification:(StimshopConnectQueryNotification *)notification __attribute__((swift_name("create(notification:)")));
- (id<StimshopKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimNotificationItem")))
@interface StimshopStimNotificationItem : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopStimNotificationItemCompanion *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL defaultNotification __attribute__((swift_name("defaultNotification")));
@property (readonly) int32_t delayBetweenDisplay __attribute__((swift_name("delayBetweenDisplay")));
@property (readonly) NSString *displayType __attribute__((swift_name("displayType")));
@property (readonly) NSString * _Nullable htmlContent __attribute__((swift_name("htmlContent")));
@property (readonly) NSString * _Nullable iconUrl __attribute__((swift_name("iconUrl")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable imageLink __attribute__((swift_name("imageLink")));
@property (readonly) NSString * _Nullable imageUrl __attribute__((swift_name("imageUrl")));
@property (readonly) int32_t numberOfDisplay __attribute__((swift_name("numberOfDisplay")));
@property (readonly) StimshopInt * _Nullable probability __attribute__((swift_name("probability")));
@property (readonly) StimshopInt * _Nullable quota __attribute__((swift_name("quota")));
@property (readonly) NSString * _Nullable videoLink __attribute__((swift_name("videoLink")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimNotificationItem.Companion")))
@interface StimshopStimNotificationItemCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopStimNotificationItemCompanion *shared __attribute__((swift_name("shared")));
- (id<StimshopKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimNotificationType")))
@interface StimshopStimNotificationType : StimshopKotlinEnum<StimshopStimNotificationType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) StimshopStimNotificationTypeCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) StimshopStimNotificationType *simple __attribute__((swift_name("simple")));
@property (class, readonly) StimshopStimNotificationType *random __attribute__((swift_name("random")));
@property (class, readonly) StimshopStimNotificationType *instantWin __attribute__((swift_name("instantWin")));
@property (class, readonly) StimshopStimNotificationType *treasureHunt __attribute__((swift_name("treasureHunt")));
+ (StimshopKotlinArray<StimshopStimNotificationType *> *)values __attribute__((swift_name("values()")));
@property (class, readonly) NSArray<StimshopStimNotificationType *> *entries __attribute__((swift_name("entries")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimNotificationType.Companion")))
@interface StimshopStimNotificationTypeCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopStimNotificationTypeCompanion *shared __attribute__((swift_name("shared")));
- (StimshopStimNotificationType *)fromValue:(NSString *)value __attribute__((swift_name("from(value:)")));
- (id<StimshopKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
- (id<StimshopKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(StimshopKotlinArray<id<StimshopKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DataStore")))
@interface StimshopDataStore : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)dataStore __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopDataStore *shared __attribute__((swift_name("shared")));
- (BOOL)clearSession __attribute__((swift_name("clearSession()")));
- (id _Nullable)loadPendingLogEvents __attribute__((swift_name("loadPendingLogEvents()")));
- (StimshopSession * _Nullable)loadSession __attribute__((swift_name("loadSession()")));
- (void)persistPendingLogEventsEvents:(id _Nullable)events __attribute__((swift_name("persistPendingLogEvents(events:)")));
- (void)persistSessionSession:(StimshopSession *)session __attribute__((swift_name("persistSession(session:)")));
@property (readonly) NSString *SESSION_KEY __attribute__((swift_name("SESSION_KEY")));
@property (readonly) StimshopSecureprefsSecurePrefs *logEventsStorage __attribute__((swift_name("logEventsStorage")));
@property (readonly) StimshopSecureprefsSecurePrefs *sessionStorage __attribute__((swift_name("sessionStorage")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LogEvent")))
@interface StimshopLogEvent : StimshopBase
- (instancetype)initWithId:(NSString *)id __attribute__((swift_name("init(id:)"))) __attribute__((objc_designated_initializer));
- (StimshopLogEvent *)doCopyId:(NSString *)id __attribute__((swift_name("doCopy(id:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@end


/**
 * @note annotations
 *   kotlinx.coroutines.DelicateCoroutinesApi
*/
__attribute__((swift_name("Bus")))
@interface StimshopBus : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)addObserverObserver:(id)observer message:(StimshopMessage<id> *)message block:(void (^)(id _Nullable))block __attribute__((swift_name("addObserver(observer:message:block:)")));
- (void)addObserverObserver:(id)observer key:(NSString *)key block:(void (^)(id _Nullable))block __attribute__((swift_name("addObserver(observer:key:block:)")));
- (void)postKey:(NSString *)key value:(id _Nullable)value __attribute__((swift_name("post(key:value:)")));
- (void)removeObserverObserver:(id)observer __attribute__((swift_name("removeObserver(observer:)")));
- (void)removeObserverObserver:(id)observer message:(StimshopMessage<id> *)message __attribute__((swift_name("removeObserver(observer:message:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Constants")))
@interface StimshopConstants : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopConstantsCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Constants.Companion")))
@interface StimshopConstantsCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopConstantsCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) uint32_t kInputBus __attribute__((swift_name("kInputBus")));
@property (readonly) uint32_t kOutputBus __attribute__((swift_name("kOutputBus")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FrequencyChannel")))
@interface StimshopFrequencyChannel : StimshopBase
- (instancetype)initWithId:(int32_t)id startFrequency:(float)startFrequency endFrequency:(float)endFrequency midFrenquency:(float)midFrenquency __attribute__((swift_name("init(id:startFrequency:endFrequency:midFrenquency:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) StimshopFrequencyChannelCompanion *companion __attribute__((swift_name("companion")));
- (StimshopFrequencyChannel *)doCopyId:(int32_t)id startFrequency:(float)startFrequency endFrequency:(float)endFrequency midFrenquency:(float)midFrenquency __attribute__((swift_name("doCopy(id:startFrequency:endFrequency:midFrenquency:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) float endFrequency __attribute__((swift_name("endFrequency")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) float midFrenquency __attribute__((swift_name("midFrenquency")));
@property (readonly) float startFrequency __attribute__((swift_name("startFrequency")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FrequencyChannel.Companion")))
@interface StimshopFrequencyChannelCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopFrequencyChannelCompanion *shared __attribute__((swift_name("shared")));
- (StimshopFrequencyChannel *)getByIdId:(int32_t)id __attribute__((swift_name("getById(id:)")));
@property (readonly) StimshopFrequencyChannel *CHANNEL_1 __attribute__((swift_name("CHANNEL_1")));
@property (readonly) StimshopFrequencyChannel *CHANNEL_WIUS __attribute__((swift_name("CHANNEL_WIUS")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MaxValIndexFloat")))
@interface StimshopMaxValIndexFloat : StimshopBase
- (instancetype)initWithMax:(float)max index:(int32_t)index __attribute__((swift_name("init(max:index:)"))) __attribute__((objc_designated_initializer));
- (StimshopMaxValIndexFloat *)doCopyMax:(float)max index:(int32_t)index __attribute__((swift_name("doCopy(max:index:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property int32_t index __attribute__((swift_name("index")));
@property float max __attribute__((swift_name("max")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PermissionsUtils")))
@interface StimshopPermissionsUtils : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)permissionsUtils __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopPermissionsUtils *shared __attribute__((swift_name("shared")));
- (BOOL)arePermissionsRequired __attribute__((swift_name("arePermissionsRequired()")));
- (BOOL)isAudioPermissionGranted __attribute__((swift_name("isAudioPermissionGranted()")));
@property (readonly) NSArray<NSString *> *REQUIRED_PERMISSIONS __attribute__((swift_name("REQUIRED_PERMISSIONS")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PlatformUtils")))
@interface StimshopPlatformUtils : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)platformUtils __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopPlatformUtils *shared __attribute__((swift_name("shared")));
- (BOOL)isDataConnectionAvailable __attribute__((swift_name("isDataConnectionAvailable()")));
@property (readonly) BOOL isDebug __attribute__((swift_name("isDebug")));
@property (readonly) NSString *modelName __attribute__((swift_name("modelName")));
@property (readonly) NSString *osName __attribute__((swift_name("osName")));
@property (readonly) NSString *uuid __attribute__((swift_name("uuid")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end


/**
 * @note annotations
 *   kotlinx.coroutines.DelicateCoroutinesApi
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StimshopBus")))
@interface StimshopStimshopBus : StimshopBus
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (instancetype)stimshopBus __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopStimshopBus *shared __attribute__((swift_name("shared")));
- (void)onStimshopErrorObserver:(id)observer block:(void (^)(StimshopStimShopError *))block __attribute__((swift_name("onStimshopError(observer:block:)")));
- (void)onStimshopEventObserver:(id)observer block:(void (^)(StimshopStimShopEvent *))block __attribute__((swift_name("onStimshopEvent(observer:block:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TSUtils")))
@interface StimshopTSUtils : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)tSUtils __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopTSUtils *shared __attribute__((swift_name("shared")));
- (StimshopMaxValIndexFloat *)findMaxIndexData:(StimshopKotlinFloatArray *)data startPos:(int32_t)startPos stopPos:(int32_t)stopPos __attribute__((swift_name("findMaxIndex(data:startPos:stopPos:)")));
- (StimshopKotlinFloatArray *)tukeyWindowT:(int32_t)t alpha:(float)alpha __attribute__((swift_name("tukeyWindow(t:alpha:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConstantsKt")))
@interface StimshopConstantsKt : StimshopBase
+ (StimshopKotlinFloatArray *)chirpFc:(float)fc b:(int32_t)b t:(int32_t)t fe:(float)fe isUp:(BOOL)isUp __attribute__((swift_name("chirp(fc:b:t:fe:isUp:)")));
+ (int32_t)pow:(int32_t)receiver n:(int32_t)n __attribute__((swift_name("pow(_:n:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("HexKt")))
@interface StimshopHexKt : StimshopBase
+ (NSString *)toHex:(int32_t)receiver __attribute__((swift_name("toHex(_:)")));
@end

__attribute__((swift_name("OkioIOException")))
@interface StimshopOkioIOException : StimshopKotlinException
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithCause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end

__attribute__((swift_name("Apollo_apiCompiledSelection")))
@interface StimshopApollo_apiCompiledSelection : StimshopBase
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiCompiledField")))
@interface StimshopApollo_apiCompiledField : StimshopApollo_apiCompiledSelection
- (NSString *)nameWithArgumentsVariables:(StimshopApollo_apiExecutableVariables *)variables __attribute__((swift_name("nameWithArguments(variables:)")));
- (StimshopApollo_apiCompiledFieldBuilder *)doNewBuilder __attribute__((swift_name("doNewBuilder()")));
- (id _Nullable)resolveArgumentName:(NSString *)name variables:(StimshopApollo_apiExecutableVariables *)variables __attribute__((swift_name("resolveArgument(name:variables:)")));
@property (readonly) NSString * _Nullable alias __attribute__((swift_name("alias")));
@property (readonly) NSArray<StimshopApollo_apiCompiledArgument *> *arguments __attribute__((swift_name("arguments")));
@property (readonly) NSArray<StimshopApollo_apiCompiledCondition *> *condition __attribute__((swift_name("condition")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *responseName __attribute__((swift_name("responseName")));
@property (readonly) NSArray<StimshopApollo_apiCompiledSelection *> *selections __attribute__((swift_name("selections")));
@property (readonly) StimshopApollo_apiCompiledType *type __attribute__((swift_name("type")));
@end

__attribute__((swift_name("OkioCloseable")))
@protocol StimshopOkioCloseable
@required

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)closeAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("close()")));
@end

__attribute__((swift_name("Apollo_apiJsonWriter")))
@protocol StimshopApollo_apiJsonWriter <StimshopOkioCloseable>
@required

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonWriter> _Nullable)beginArrayAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("beginArray()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonWriter> _Nullable)beginObjectAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("beginObject()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonWriter> _Nullable)endArrayAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("endArray()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonWriter> _Nullable)endObjectAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("endObject()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)flushAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("flush()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonWriter> _Nullable)nameName:(NSString *)name error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("name(name:)")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonWriter> _Nullable)nullValueAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("nullValue()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonWriter> _Nullable)valueValue:(id<StimshopApollo_apiUpload>)value error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("value(value:)")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonWriter> _Nullable)valueValue:(StimshopApollo_apiJsonNumber *)value error_:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("value(value_:)")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonWriter> _Nullable)valueValue:(BOOL)value error__:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("value(value__:)")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonWriter> _Nullable)valueValue:(double)value error___:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("value(value___:)")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonWriter> _Nullable)valueValue:(int32_t)value error____:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("value(value____:)")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonWriter> _Nullable)valueValue:(int64_t)value error_____:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("value(value_____:)")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonWriter> _Nullable)valueValue:(NSString *)value error______:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("value(value______:)")));
@property (readonly) NSString *path __attribute__((swift_name("path")));
@end

__attribute__((swift_name("Apollo_apiExecutionContext")))
@protocol StimshopApollo_apiExecutionContext
@required
- (id _Nullable)foldInitial:(id _Nullable)initial operation:(id _Nullable (^)(id _Nullable, id<StimshopApollo_apiExecutionContextElement>))operation __attribute__((swift_name("fold(initial:operation:)")));
- (id<StimshopApollo_apiExecutionContextElement> _Nullable)getKey:(id<StimshopApollo_apiExecutionContextKey>)key __attribute__((swift_name("get(key:)")));
- (id<StimshopApollo_apiExecutionContext>)minusKeyKey:(id<StimshopApollo_apiExecutionContextKey>)key __attribute__((swift_name("minusKey(key:)")));
- (id<StimshopApollo_apiExecutionContext>)plusContext:(id<StimshopApollo_apiExecutionContext>)context __attribute__((swift_name("plus(context:)")));
@end

__attribute__((swift_name("Apollo_apiExecutionContextElement")))
@protocol StimshopApollo_apiExecutionContextElement <StimshopApollo_apiExecutionContext>
@required
@property (readonly) id<StimshopApollo_apiExecutionContextKey> key __attribute__((swift_name("key")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiCustomScalarAdapters")))
@interface StimshopApollo_apiCustomScalarAdapters : StimshopBase <StimshopApollo_apiExecutionContextElement>
@property (class, readonly, getter=companion) StimshopApollo_apiCustomScalarAdaptersKey *companion __attribute__((swift_name("companion")));
- (StimshopApollo_apiCustomScalarAdaptersBuilder *)doNewBuilder __attribute__((swift_name("doNewBuilder()")));
- (id<StimshopApollo_apiAdapter>)responseAdapterForCustomScalar:(StimshopApollo_apiCustomScalarType *)customScalar __attribute__((swift_name("responseAdapterFor(customScalar:)")));
- (NSSet<NSString *> *)variables __attribute__((swift_name("variables()"))) __attribute__((deprecated("Use adapterContext.variables() instead")));
@property (readonly) StimshopApollo_apiAdapterContext *adapterContext __attribute__((swift_name("adapterContext")));
@property (readonly) id<StimshopApollo_apiExecutionContextKey> key __attribute__((swift_name("key")));
@end

__attribute__((swift_name("KotlinIllegalStateException")))
@interface StimshopKotlinIllegalStateException : StimshopKotlinRuntimeException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.4")
*/
__attribute__((swift_name("KotlinCancellationException")))
@interface StimshopKotlinCancellationException : StimshopKotlinIllegalStateException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(StimshopKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((swift_name("Apollo_apiJsonReader")))
@protocol StimshopApollo_apiJsonReader <StimshopOkioCloseable>
@required

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonReader> _Nullable)beginArrayAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("beginArray()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonReader> _Nullable)beginObjectAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("beginObject()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonReader> _Nullable)endArrayAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("endArray()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (id<StimshopApollo_apiJsonReader> _Nullable)endObjectAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("endObject()")));
- (NSArray<id> *)getPath __attribute__((swift_name("getPath()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)hasNextAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("hasNext()"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)nextBooleanAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("nextBoolean()"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (double)nextDoubleAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("nextDouble()"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (int32_t)nextIntAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("nextInt()"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (int64_t)nextLongAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("nextLong()"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (NSString * _Nullable)nextNameAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("nextName()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopKotlinNothing * _Nullable)nextNullAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("nextNull()"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopApollo_apiJsonNumber * _Nullable)nextNumberAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("nextNumber()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (NSString * _Nullable)nextStringAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("nextString()"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (StimshopApollo_apiJsonReaderToken * _Nullable)peekAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("peek()")));
- (void)rewind __attribute__((swift_name("rewind()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (int32_t)selectNameNames:(NSArray<NSString *> *)names error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("selectName(names:)"))) __attribute__((swift_error(nonnull_error)));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)skipValueAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("skipValue()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface StimshopKotlinArray<T> : StimshopBase
+ (instancetype)arrayWithSize:(int32_t)size init:(T _Nullable (^)(StimshopInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (T _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<StimshopKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(T _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end

__attribute__((swift_name("Apollo_apiCompiledType")))
@interface StimshopApollo_apiCompiledType : StimshopBase
- (StimshopApollo_apiCompiledNamedType *)leafType __attribute__((swift_name("leafType()"))) __attribute__((deprecated("Use rawType instead")));
- (StimshopApollo_apiCompiledNamedType *)rawType __attribute__((swift_name("rawType()")));
@end

__attribute__((swift_name("Apollo_apiCompiledNamedType")))
@interface StimshopApollo_apiCompiledNamedType : StimshopApollo_apiCompiledType
- (StimshopApollo_apiCompiledNamedType *)leafType __attribute__((swift_name("leafType()"))) __attribute__((deprecated("Use rawType instead")));
- (StimshopApollo_apiCompiledNamedType *)rawType __attribute__((swift_name("rawType()")));
@property (readonly, getter=name_) NSString *name __attribute__((swift_name("name")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiCustomScalarType")))
@interface StimshopApollo_apiCustomScalarType : StimshopApollo_apiCompiledNamedType
- (instancetype)initWithName:(NSString *)name className:(NSString *)className __attribute__((swift_name("init(name:className:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *className __attribute__((swift_name("className")));
@end

__attribute__((swift_name("Apollo_apiOptional")))
@interface StimshopApollo_apiOptional<__covariant V> : StimshopBase
@property (class, readonly, getter=companion) StimshopApollo_apiOptionalCompanion *companion __attribute__((swift_name("companion")));
- (V _Nullable)getOrNull __attribute__((swift_name("getOrNull()")));
- (V)getOrThrow __attribute__((swift_name("getOrThrow()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiObjectType")))
@interface StimshopApollo_apiObjectType : StimshopApollo_apiCompiledNamedType
- (instancetype)initWithName:(NSString *)name keyFields:(NSArray<NSString *> *)keyFields implements:(NSArray<StimshopApollo_apiInterfaceType *> *)implements __attribute__((swift_name("init(name:keyFields:implements:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Use the Builder instead")));
- (StimshopApollo_apiObjectTypeBuilder *)doNewBuilder __attribute__((swift_name("doNewBuilder()")));
@property (readonly) NSArray<NSString *> *embeddedFields __attribute__((swift_name("embeddedFields")));
@property (readonly) NSArray<StimshopApollo_apiInterfaceType *> *implements __attribute__((swift_name("implements")));
@property (readonly) NSArray<NSString *> *keyFields __attribute__((swift_name("keyFields")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiUnionType")))
@interface StimshopApollo_apiUnionType : StimshopApollo_apiCompiledNamedType
- (instancetype)initWithName:(NSString *)name members:(StimshopKotlinArray<StimshopApollo_apiObjectType *> *)members __attribute__((swift_name("init(name:members:)"))) __attribute__((objc_designated_initializer));
@property (readonly) StimshopKotlinArray<StimshopApollo_apiObjectType *> *members __attribute__((swift_name("members")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinFloatArray")))
@interface StimshopKotlinFloatArray : StimshopBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(StimshopFloat *(^)(StimshopInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (float)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (StimshopKotlinFloatIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(float)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinPair")))
@interface StimshopKotlinPair<__covariant A, __covariant B> : StimshopBase
- (instancetype)initWithFirst:(A _Nullable)first second:(B _Nullable)second __attribute__((swift_name("init(first:second:)"))) __attribute__((objc_designated_initializer));
- (StimshopKotlinPair<A, B> *)doCopyFirst:(A _Nullable)first second:(B _Nullable)second __attribute__((swift_name("doCopy(first:second:)")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@property (readonly) A _Nullable first __attribute__((swift_name("first")));
@property (readonly) B _Nullable second __attribute__((swift_name("second")));
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.9")
*/
__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinAtomicReference")))
@interface StimshopKotlinAtomicReference<T> : StimshopBase
- (instancetype)initWithValue:(T _Nullable)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
- (T _Nullable)compareAndExchangeExpected:(T _Nullable)expected newValue:(T _Nullable)newValue __attribute__((swift_name("compareAndExchange(expected:newValue:)")));
- (BOOL)compareAndSetExpected:(T _Nullable)expected newValue:(T _Nullable)newValue __attribute__((swift_name("compareAndSet(expected:newValue:)")));
- (T _Nullable)getAndSetNewValue:(T _Nullable)newValue __attribute__((swift_name("getAndSet(newValue:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property T _Nullable value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinEnumCompanion")))
@interface StimshopKotlinEnumCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopKotlinEnumCompanion *shared __attribute__((swift_name("shared")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerializationStrategy")))
@protocol StimshopKotlinx_serialization_coreSerializationStrategy
@required
- (void)serializeEncoder:(id<StimshopKotlinx_serialization_coreEncoder>)encoder value:(id _Nullable)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<StimshopKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreDeserializationStrategy")))
@protocol StimshopKotlinx_serialization_coreDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<StimshopKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
@property (readonly) id<StimshopKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreKSerializer")))
@protocol StimshopKotlinx_serialization_coreKSerializer <StimshopKotlinx_serialization_coreSerializationStrategy, StimshopKotlinx_serialization_coreDeserializationStrategy>
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_datetimeInstant")))
@interface StimshopKotlinx_datetimeInstant : StimshopBase <StimshopKotlinComparable>
@property (class, readonly, getter=companion) StimshopKotlinx_datetimeInstantCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(StimshopKotlinx_datetimeInstant *)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (StimshopKotlinx_datetimeInstant *)minusDuration:(int64_t)duration __attribute__((swift_name("minus(duration:)")));
- (int64_t)minusOther:(StimshopKotlinx_datetimeInstant *)other __attribute__((swift_name("minus(other:)")));
- (StimshopKotlinx_datetimeInstant *)plusDuration:(int64_t)duration __attribute__((swift_name("plus(duration:)")));
- (int64_t)toEpochMilliseconds __attribute__((swift_name("toEpochMilliseconds()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t epochSeconds __attribute__((swift_name("epochSeconds")));
@property (readonly) int32_t nanosecondsOfSecond __attribute__((swift_name("nanosecondsOfSecond")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SecureprefsSecurePrefs")))
@interface StimshopSecureprefsSecurePrefs : StimshopBase
- (instancetype)initWithName:(NSString * _Nullable)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (void)clear __attribute__((swift_name("clear()")));
- (BOOL)containsKey:(NSString *)key __attribute__((swift_name("contains(key:)")));
- (NSDictionary<NSString *, id> *)getAll __attribute__((swift_name("getAll()")));
- (BOOL)getBooleanKey:(NSString *)key defaultValue:(BOOL)defaultValue __attribute__((swift_name("getBoolean(key:defaultValue:)")));
- (double)getDoubleKey:(NSString *)key defaultValue:(double)defaultValue __attribute__((swift_name("getDouble(key:defaultValue:)")));
- (float)getFloatKey:(NSString *)key defaultValue:(float)defaultValue __attribute__((swift_name("getFloat(key:defaultValue:)")));
- (int32_t)getIntKey:(NSString *)key defaultValue:(int32_t)defaultValue __attribute__((swift_name("getInt(key:defaultValue:)")));
- (int64_t)getLongKey:(NSString *)key defaultValue:(int64_t)defaultValue __attribute__((swift_name("getLong(key:defaultValue:)")));
- (NSString * _Nullable)getStringKey:(NSString *)key defaultValue:(NSString * _Nullable)defaultValue __attribute__((swift_name("getString(key:defaultValue:)")));
- (void)putBooleanKey:(NSString *)key value:(BOOL)value __attribute__((swift_name("putBoolean(key:value:)")));
- (void)putDoubleKey:(NSString *)key value:(double)value __attribute__((swift_name("putDouble(key:value:)")));
- (void)putFloatKey:(NSString *)key value:(float)value __attribute__((swift_name("putFloat(key:value:)")));
- (void)putIntKey:(NSString *)key value:(int32_t)value __attribute__((swift_name("putInt(key:value:)")));
- (void)putLongKey:(NSString *)key value:(int64_t)value __attribute__((swift_name("putLong(key:value:)")));
- (void)putStringKey:(NSString *)key value:(NSString *)value __attribute__((swift_name("putString(key:value:)")));
- (void)removeKey:(NSString *)key __attribute__((swift_name("remove(key:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiExecutableVariables")))
@interface StimshopApollo_apiExecutableVariables : StimshopBase
- (instancetype)initWithValueMap:(NSDictionary<NSString *, id> *)valueMap __attribute__((swift_name("init(valueMap:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSDictionary<NSString *, id> *valueMap __attribute__((swift_name("valueMap")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiCompiledField.Builder")))
@interface StimshopApollo_apiCompiledFieldBuilder : StimshopBase
- (instancetype)initWithCompiledField:(StimshopApollo_apiCompiledField *)compiledField __attribute__((swift_name("init(compiledField:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithName:(NSString *)name type:(StimshopApollo_apiCompiledType *)type __attribute__((swift_name("init(name:type:)"))) __attribute__((objc_designated_initializer));
- (StimshopApollo_apiCompiledFieldBuilder *)aliasAlias:(NSString * _Nullable)alias __attribute__((swift_name("alias(alias:)")));
- (StimshopApollo_apiCompiledFieldBuilder *)argumentsArguments:(NSArray<StimshopApollo_apiCompiledArgument *> *)arguments __attribute__((swift_name("arguments(arguments:)")));
- (StimshopApollo_apiCompiledField *)build __attribute__((swift_name("build()")));
- (StimshopApollo_apiCompiledFieldBuilder *)conditionCondition:(NSArray<StimshopApollo_apiCompiledCondition *> *)condition __attribute__((swift_name("condition(condition:)")));
- (StimshopApollo_apiCompiledFieldBuilder *)selectionsSelections:(NSArray<StimshopApollo_apiCompiledSelection *> *)selections __attribute__((swift_name("selections(selections:)")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) StimshopApollo_apiCompiledType *type __attribute__((swift_name("type")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiCompiledArgument")))
@interface StimshopApollo_apiCompiledArgument : StimshopBase
- (instancetype)initWithName:(NSString *)name value:(id _Nullable)value isKey:(BOOL)isKey __attribute__((swift_name("init(name:value:isKey:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Use the Builder instead")));
@property (readonly) BOOL isKey __attribute__((swift_name("isKey")));

/**
 * @note annotations
 *   com.apollographql.apollo3.annotations.ApolloExperimental
*/
@property (readonly) BOOL isPagination __attribute__((swift_name("isPagination")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) id _Nullable value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiCompiledCondition")))
@interface StimshopApollo_apiCompiledCondition : StimshopBase
- (instancetype)initWithName:(NSString *)name inverted:(BOOL)inverted __attribute__((swift_name("init(name:inverted:)"))) __attribute__((objc_designated_initializer));
- (StimshopApollo_apiCompiledCondition *)doCopyName:(NSString *)name inverted:(BOOL)inverted __attribute__((swift_name("doCopy(name:inverted:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL inverted __attribute__((swift_name("inverted")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end

__attribute__((swift_name("Apollo_apiUpload")))
@protocol StimshopApollo_apiUpload
@required
- (void)writeToSink:(id<StimshopOkioBufferedSink>)sink __attribute__((swift_name("writeTo(sink:)")));
@property (readonly) int64_t contentLength __attribute__((swift_name("contentLength")));
@property (readonly) NSString *contentType __attribute__((swift_name("contentType")));
@property (readonly) NSString * _Nullable fileName __attribute__((swift_name("fileName")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiJsonNumber")))
@interface StimshopApollo_apiJsonNumber : StimshopBase
- (instancetype)initWithValue:(NSString *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end

__attribute__((swift_name("Apollo_apiExecutionContextKey")))
@protocol StimshopApollo_apiExecutionContextKey
@required
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiCustomScalarAdapters.Key")))
@interface StimshopApollo_apiCustomScalarAdaptersKey : StimshopBase <StimshopApollo_apiExecutionContextKey>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)key __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopApollo_apiCustomScalarAdaptersKey *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopApollo_apiCustomScalarAdapters *Empty __attribute__((swift_name("Empty")));

/**
 * @note annotations
 *   com.apollographql.apollo3.annotations.ApolloExperimental
*/
@property (readonly) StimshopApollo_apiCustomScalarAdapters *PassThrough __attribute__((swift_name("PassThrough")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiCustomScalarAdapters.Builder")))
@interface StimshopApollo_apiCustomScalarAdaptersBuilder : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (StimshopApollo_apiCustomScalarAdaptersBuilder *)adapterContextAdapterContext:(StimshopApollo_apiAdapterContext *)adapterContext __attribute__((swift_name("adapterContext(adapterContext:)")));
- (StimshopApollo_apiCustomScalarAdaptersBuilder *)addCustomScalarType:(StimshopApollo_apiCustomScalarType *)customScalarType customScalarAdapter:(id<StimshopApollo_apiAdapter>)customScalarAdapter __attribute__((swift_name("add(customScalarType:customScalarAdapter:)")));
- (StimshopApollo_apiCustomScalarAdaptersBuilder *)addCustomScalarType:(StimshopApollo_apiCustomScalarType *)customScalarType customTypeAdapter:(id<StimshopApollo_apiCustomTypeAdapter>)customTypeAdapter __attribute__((swift_name("add(customScalarType:customTypeAdapter:)"))) __attribute__((deprecated("Used for backward compatibility with 2.x")));
- (StimshopApollo_apiCustomScalarAdaptersBuilder *)addAllCustomScalarAdapters:(StimshopApollo_apiCustomScalarAdapters *)customScalarAdapters __attribute__((swift_name("addAll(customScalarAdapters:)")));
- (StimshopApollo_apiCustomScalarAdapters *)build __attribute__((swift_name("build()")));
- (void)clear __attribute__((swift_name("clear()")));

/**
 * @note annotations
 *   com.apollographql.apollo3.annotations.ApolloExperimental
*/
- (StimshopApollo_apiCustomScalarAdaptersBuilder *)unsafeUnsafe:(BOOL)unsafe __attribute__((swift_name("unsafe(unsafe:)")));
- (StimshopApollo_apiCustomScalarAdaptersBuilder *)variablesVariables:(StimshopApollo_apiExecutableVariables *)variables __attribute__((swift_name("variables(variables:)"))) __attribute__((deprecated("Use AdapterContext.Builder.variables() instead")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiAdapterContext")))
@interface StimshopApollo_apiAdapterContext : StimshopBase
- (BOOL)hasDeferredFragmentPath:(NSArray<id> *)path label:(NSString * _Nullable)label __attribute__((swift_name("hasDeferredFragment(path:label:)")));
- (StimshopApollo_apiAdapterContextBuilder *)doNewBuilder __attribute__((swift_name("doNewBuilder()")));
- (NSSet<NSString *> *)variables __attribute__((swift_name("variables()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface StimshopKotlinNothing : StimshopBase
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiJsonReaderToken")))
@interface StimshopApollo_apiJsonReaderToken : StimshopKotlinEnum<StimshopApollo_apiJsonReaderToken *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) StimshopApollo_apiJsonReaderToken *beginArray __attribute__((swift_name("beginArray")));
@property (class, readonly) StimshopApollo_apiJsonReaderToken *endArray __attribute__((swift_name("endArray")));
@property (class, readonly) StimshopApollo_apiJsonReaderToken *beginObject __attribute__((swift_name("beginObject")));
@property (class, readonly) StimshopApollo_apiJsonReaderToken *endObject __attribute__((swift_name("endObject")));
@property (class, readonly) StimshopApollo_apiJsonReaderToken *name __attribute__((swift_name("name")));
@property (class, readonly) StimshopApollo_apiJsonReaderToken *string __attribute__((swift_name("string")));
@property (class, readonly) StimshopApollo_apiJsonReaderToken *number __attribute__((swift_name("number")));
@property (class, readonly) StimshopApollo_apiJsonReaderToken *long_ __attribute__((swift_name("long_")));
@property (class, readonly) StimshopApollo_apiJsonReaderToken *boolean __attribute__((swift_name("boolean")));
@property (class, readonly) StimshopApollo_apiJsonReaderToken *null __attribute__((swift_name("null")));
@property (class, readonly) StimshopApollo_apiJsonReaderToken *endDocument __attribute__((swift_name("endDocument")));
@property (class, readonly) StimshopApollo_apiJsonReaderToken *any __attribute__((swift_name("any")));
+ (StimshopKotlinArray<StimshopApollo_apiJsonReaderToken *> *)values __attribute__((swift_name("values()")));
@end

__attribute__((swift_name("KotlinIterator")))
@protocol StimshopKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext_()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiOptionalCompanion")))
@interface StimshopApollo_apiOptionalCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopApollo_apiOptionalCompanion *shared __attribute__((swift_name("shared")));

/**
 * @note annotations
 *   kotlin.jvm.JvmStatic
*/
- (StimshopApollo_apiOptional<id> *)absent __attribute__((swift_name("absent()")));

/**
 * @note annotations
 *   kotlin.jvm.JvmStatic
*/
- (StimshopApollo_apiOptional<id> *)presentValue:(id _Nullable)value __attribute__((swift_name("present(value:)")));

/**
 * @note annotations
 *   kotlin.jvm.JvmStatic
*/
- (StimshopApollo_apiOptional<id> *)presentIfNotNullValue:(id _Nullable)value __attribute__((swift_name("presentIfNotNull(value:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiInterfaceType")))
@interface StimshopApollo_apiInterfaceType : StimshopApollo_apiCompiledNamedType
- (instancetype)initWithName:(NSString *)name keyFields:(NSArray<NSString *> *)keyFields implements:(NSArray<StimshopApollo_apiInterfaceType *> *)implements __attribute__((swift_name("init(name:keyFields:implements:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Use the Builder instead")));
- (StimshopApollo_apiInterfaceTypeBuilder *)doNewBuilder __attribute__((swift_name("doNewBuilder()")));
@property (readonly) NSArray<NSString *> *embeddedFields __attribute__((swift_name("embeddedFields")));
@property (readonly) NSArray<StimshopApollo_apiInterfaceType *> *implements __attribute__((swift_name("implements")));
@property (readonly) NSArray<NSString *> *keyFields __attribute__((swift_name("keyFields")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiObjectType.Builder")))
@interface StimshopApollo_apiObjectTypeBuilder : StimshopBase
- (instancetype)initWithObjectType:(StimshopApollo_apiObjectType *)objectType __attribute__((swift_name("init(objectType:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (StimshopApollo_apiObjectType *)build __attribute__((swift_name("build()")));
- (StimshopApollo_apiObjectTypeBuilder *)embeddedFieldsEmbeddedFields:(NSArray<NSString *> *)embeddedFields __attribute__((swift_name("embeddedFields(embeddedFields:)")));
- (StimshopApollo_apiObjectTypeBuilder *)interfacesImplements:(NSArray<StimshopApollo_apiInterfaceType *> *)implements __attribute__((swift_name("interfaces(implements:)")));
- (StimshopApollo_apiObjectTypeBuilder *)keyFieldsKeyFields:(NSArray<NSString *> *)keyFields __attribute__((swift_name("keyFields(keyFields:)")));
@end

__attribute__((swift_name("KotlinFloatIterator")))
@interface StimshopKotlinFloatIterator : StimshopBase <StimshopKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (StimshopFloat *)next __attribute__((swift_name("next()")));
- (float)nextFloat __attribute__((swift_name("nextFloat()")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreEncoder")))
@protocol StimshopKotlinx_serialization_coreEncoder
@required
- (id<StimshopKotlinx_serialization_coreCompositeEncoder>)beginCollectionDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor collectionSize:(int32_t)collectionSize __attribute__((swift_name("beginCollection(descriptor:collectionSize:)")));
- (id<StimshopKotlinx_serialization_coreCompositeEncoder>)beginStructureDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)enumDescriptor index:(int32_t)index __attribute__((swift_name("encodeEnum(enumDescriptor:index:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (id<StimshopKotlinx_serialization_coreEncoder>)encodeInlineInlineDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("encodeInline(inlineDescriptor:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));
- (void)encodeNull __attribute__((swift_name("encodeNull()")));
- (void)encodeNullableSerializableValueSerializer:(id<StimshopKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<StimshopKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
@property (readonly) StimshopKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerialDescriptor")))
@protocol StimshopKotlinx_serialization_coreSerialDescriptor
@required
- (NSArray<id<StimshopKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));
- (id<StimshopKotlinx_serialization_coreSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));
@property (readonly) NSArray<id<StimshopKotlinAnnotation>> *annotations __attribute__((swift_name("annotations")));
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isInline __attribute__((swift_name("isInline")));
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) StimshopKotlinx_serialization_coreSerialKind *kind __attribute__((swift_name("kind")));
@property (readonly) NSString *serialName __attribute__((swift_name("serialName")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreDecoder")))
@protocol StimshopKotlinx_serialization_coreDecoder
@required
- (id<StimshopKotlinx_serialization_coreCompositeDecoder>)beginStructureDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)enumDescriptor __attribute__((swift_name("decodeEnum(enumDescriptor:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (id<StimshopKotlinx_serialization_coreDecoder>)decodeInlineInlineDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("decodeInline(inlineDescriptor:)")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));
- (StimshopKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<StimshopKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<StimshopKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
@property (readonly) StimshopKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_datetimeInstant.Companion")))
@interface StimshopKotlinx_datetimeInstantCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopKotlinx_datetimeInstantCompanion *shared __attribute__((swift_name("shared")));
- (StimshopKotlinx_datetimeInstant *)fromEpochMillisecondsEpochMilliseconds:(int64_t)epochMilliseconds __attribute__((swift_name("fromEpochMilliseconds(epochMilliseconds:)")));
- (StimshopKotlinx_datetimeInstant *)fromEpochSecondsEpochSeconds:(int64_t)epochSeconds nanosecondAdjustment:(int32_t)nanosecondAdjustment __attribute__((swift_name("fromEpochSeconds(epochSeconds:nanosecondAdjustment:)")));
- (StimshopKotlinx_datetimeInstant *)fromEpochSecondsEpochSeconds:(int64_t)epochSeconds nanosecondAdjustment_:(int64_t)nanosecondAdjustment __attribute__((swift_name("fromEpochSeconds(epochSeconds:nanosecondAdjustment_:)")));
- (StimshopKotlinx_datetimeInstant *)now __attribute__((swift_name("now()"))) __attribute__((unavailable("Use Clock.System.now() instead")));
- (StimshopKotlinx_datetimeInstant *)parseIsoString:(NSString *)isoString __attribute__((swift_name("parse(isoString:)")));
- (id<StimshopKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@property (readonly) StimshopKotlinx_datetimeInstant *DISTANT_FUTURE __attribute__((swift_name("DISTANT_FUTURE")));
@property (readonly) StimshopKotlinx_datetimeInstant *DISTANT_PAST __attribute__((swift_name("DISTANT_PAST")));
@end

__attribute__((swift_name("OkioSink")))
@protocol StimshopOkioSink <StimshopOkioCloseable>
@required

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)flushAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("flush()")));
- (StimshopOkioTimeout *)timeout __attribute__((swift_name("timeout()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)writeSource:(StimshopOkioBuffer *)source byteCount:(int64_t)byteCount error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("write(source:byteCount_:)")));
@end

__attribute__((swift_name("OkioBufferedSink")))
@protocol StimshopOkioBufferedSink <StimshopOkioSink>
@required
- (id<StimshopOkioBufferedSink>)emit __attribute__((swift_name("emit()")));
- (id<StimshopOkioBufferedSink>)emitCompleteSegments __attribute__((swift_name("emitCompleteSegments()")));
- (id<StimshopOkioBufferedSink>)writeSource:(StimshopKotlinByteArray *)source __attribute__((swift_name("write(source:)")));
- (id<StimshopOkioBufferedSink>)writeSource:(StimshopKotlinByteArray *)source offset:(int32_t)offset byteCount:(int32_t)byteCount __attribute__((swift_name("write(source:offset:byteCount:)")));
- (id<StimshopOkioBufferedSink>)writeByteString:(StimshopOkioByteString *)byteString __attribute__((swift_name("write(byteString:)")));
- (id<StimshopOkioBufferedSink>)writeByteString:(StimshopOkioByteString *)byteString offset:(int32_t)offset byteCount:(int32_t)byteCount __attribute__((swift_name("write(byteString:offset:byteCount:)")));
- (id<StimshopOkioBufferedSink>)writeSource:(id<StimshopOkioSource>)source byteCount:(int64_t)byteCount __attribute__((swift_name("write(source:byteCount:)")));
- (int64_t)writeAllSource:(id<StimshopOkioSource>)source __attribute__((swift_name("writeAll(source:)")));
- (id<StimshopOkioBufferedSink>)writeByteB:(int32_t)b __attribute__((swift_name("writeByte(b:)")));
- (id<StimshopOkioBufferedSink>)writeDecimalLongV:(int64_t)v __attribute__((swift_name("writeDecimalLong(v:)")));
- (id<StimshopOkioBufferedSink>)writeHexadecimalUnsignedLongV:(int64_t)v __attribute__((swift_name("writeHexadecimalUnsignedLong(v:)")));
- (id<StimshopOkioBufferedSink>)writeIntI:(int32_t)i __attribute__((swift_name("writeInt(i:)")));
- (id<StimshopOkioBufferedSink>)writeIntLeI:(int32_t)i __attribute__((swift_name("writeIntLe(i:)")));
- (id<StimshopOkioBufferedSink>)writeLongV:(int64_t)v __attribute__((swift_name("writeLong(v:)")));
- (id<StimshopOkioBufferedSink>)writeLongLeV:(int64_t)v __attribute__((swift_name("writeLongLe(v:)")));
- (id<StimshopOkioBufferedSink>)writeShortS:(int32_t)s __attribute__((swift_name("writeShort(s:)")));
- (id<StimshopOkioBufferedSink>)writeShortLeS:(int32_t)s __attribute__((swift_name("writeShortLe(s:)")));
- (id<StimshopOkioBufferedSink>)writeUtf8String:(NSString *)string __attribute__((swift_name("writeUtf8(string:)")));
- (id<StimshopOkioBufferedSink>)writeUtf8String:(NSString *)string beginIndex:(int32_t)beginIndex endIndex:(int32_t)endIndex __attribute__((swift_name("writeUtf8(string:beginIndex:endIndex:)")));
- (id<StimshopOkioBufferedSink>)writeUtf8CodePointCodePoint:(int32_t)codePoint __attribute__((swift_name("writeUtf8CodePoint(codePoint:)")));
@property (readonly) StimshopOkioBuffer *buffer __attribute__((swift_name("buffer")));
@end

__attribute__((swift_name("Apollo_apiCustomTypeAdapter")))
@protocol StimshopApollo_apiCustomTypeAdapter
@required
- (id _Nullable)decodeValue:(StimshopApollo_apiCustomTypeValue<id> *)value __attribute__((swift_name("decode(value:)")));
- (StimshopApollo_apiCustomTypeValue<id> *)encodeValue:(id _Nullable)value __attribute__((swift_name("encode(value:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiAdapterContext.Builder")))
@interface StimshopApollo_apiAdapterContextBuilder : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (StimshopApollo_apiAdapterContext *)build __attribute__((swift_name("build()")));
- (StimshopApollo_apiAdapterContextBuilder *)mergedDeferredFragmentIdsMergedDeferredFragmentIds:(NSSet<StimshopApollo_apiDeferredFragmentIdentifier *> * _Nullable)mergedDeferredFragmentIds __attribute__((swift_name("mergedDeferredFragmentIds(mergedDeferredFragmentIds:)")));
- (StimshopApollo_apiAdapterContextBuilder *)variablesVariables:(StimshopApollo_apiExecutableVariables * _Nullable)variables __attribute__((swift_name("variables(variables:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiInterfaceType.Builder")))
@interface StimshopApollo_apiInterfaceTypeBuilder : StimshopBase
- (instancetype)initWithInterfaceType:(StimshopApollo_apiInterfaceType *)interfaceType __attribute__((swift_name("init(interfaceType:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (StimshopApollo_apiInterfaceType *)build __attribute__((swift_name("build()")));
- (StimshopApollo_apiInterfaceTypeBuilder *)embeddedFieldsEmbeddedFields:(NSArray<NSString *> *)embeddedFields __attribute__((swift_name("embeddedFields(embeddedFields:)")));
- (StimshopApollo_apiInterfaceTypeBuilder *)interfacesImplements:(NSArray<StimshopApollo_apiInterfaceType *> *)implements __attribute__((swift_name("interfaces(implements:)")));
- (StimshopApollo_apiInterfaceTypeBuilder *)keyFieldsKeyFields:(NSArray<NSString *> *)keyFields __attribute__((swift_name("keyFields(keyFields:)")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreCompositeEncoder")))
@protocol StimshopKotlinx_serialization_coreCompositeEncoder
@required
- (void)encodeBooleanElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(descriptor:index:value:)")));
- (void)encodeByteElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(descriptor:index:value:)")));
- (void)encodeCharElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(descriptor:index:value:)")));
- (void)encodeDoubleElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(descriptor:index:value:)")));
- (void)encodeFloatElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(descriptor:index:value:)")));
- (id<StimshopKotlinx_serialization_coreEncoder>)encodeInlineElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("encodeInlineElement(descriptor:index:)")));
- (void)encodeIntElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(descriptor:index:value:)")));
- (void)encodeLongElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(descriptor:index:value:)")));
- (void)encodeNullableSerializableElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<StimshopKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeSerializableElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<StimshopKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeShortElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(descriptor:index:value:)")));
- (void)encodeStringElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(descriptor:index:value:)")));
- (void)endStructureDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (BOOL)shouldEncodeElementDefaultDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(descriptor:index:)")));
@property (readonly) StimshopKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModule")))
@interface StimshopKotlinx_serialization_coreSerializersModule : StimshopBase
- (void)dumpToCollector:(id<StimshopKotlinx_serialization_coreSerializersModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));
- (id<StimshopKotlinx_serialization_coreKSerializer> _Nullable)getContextualKClass:(id<StimshopKotlinKClass>)kClass typeArgumentsSerializers:(NSArray<id<StimshopKotlinx_serialization_coreKSerializer>> *)typeArgumentsSerializers __attribute__((swift_name("getContextual(kClass:typeArgumentsSerializers:)")));
- (id<StimshopKotlinx_serialization_coreSerializationStrategy> _Nullable)getPolymorphicBaseClass:(id<StimshopKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));
- (id<StimshopKotlinx_serialization_coreDeserializationStrategy> _Nullable)getPolymorphicBaseClass:(id<StimshopKotlinKClass>)baseClass serializedClassName:(NSString * _Nullable)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end

__attribute__((swift_name("KotlinAnnotation")))
@protocol StimshopKotlinAnnotation
@required
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerialKind")))
@interface StimshopKotlinx_serialization_coreSerialKind : StimshopBase
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreCompositeDecoder")))
@protocol StimshopKotlinx_serialization_coreCompositeDecoder
@required
- (BOOL)decodeBooleanElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(descriptor:index:)")));
- (int8_t)decodeByteElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeByteElement(descriptor:index:)")));
- (unichar)decodeCharElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeCharElement(descriptor:index:)")));
- (int32_t)decodeCollectionSizeDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeCollectionSize(descriptor:)")));
- (double)decodeDoubleElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(descriptor:index:)")));
- (int32_t)decodeElementIndexDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeElementIndex(descriptor:)")));
- (float)decodeFloatElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeFloatElement(descriptor:index:)")));
- (id<StimshopKotlinx_serialization_coreDecoder>)decodeInlineElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeInlineElement(descriptor:index:)")));
- (int32_t)decodeIntElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeIntElement(descriptor:index:)")));
- (int64_t)decodeLongElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeLongElement(descriptor:index:)")));
- (id _Nullable)decodeNullableSerializableElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<StimshopKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeNullableSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (BOOL)decodeSequentially __attribute__((swift_name("decodeSequentially()")));
- (id _Nullable)decodeSerializableElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<StimshopKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (int16_t)decodeShortElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeShortElement(descriptor:index:)")));
- (NSString *)decodeStringElementDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeStringElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<StimshopKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
@property (readonly) StimshopKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinByteArray")))
@interface StimshopKotlinByteArray : StimshopBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(StimshopByte *(^)(StimshopInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (int8_t)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (StimshopKotlinByteIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end

__attribute__((swift_name("OkioByteString")))
@interface StimshopOkioByteString : StimshopBase <StimshopKotlinComparable>
@property (class, readonly, getter=companion) StimshopOkioByteStringCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)base64 __attribute__((swift_name("base64()")));
- (NSString *)base64Url __attribute__((swift_name("base64Url()")));
- (int32_t)compareToOther:(StimshopOkioByteString *)other __attribute__((swift_name("compareTo(other:)")));
- (void)doCopyIntoOffset:(int32_t)offset target:(StimshopKotlinByteArray *)target targetOffset:(int32_t)targetOffset byteCount:(int32_t)byteCount __attribute__((swift_name("doCopyInto(offset:target:targetOffset:byteCount:)")));
- (BOOL)endsWithSuffix:(StimshopKotlinByteArray *)suffix __attribute__((swift_name("endsWith(suffix:)")));
- (BOOL)endsWithSuffix_:(StimshopOkioByteString *)suffix __attribute__((swift_name("endsWith(suffix_:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (int8_t)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)hex __attribute__((swift_name("hex()")));
- (StimshopOkioByteString *)hmacSha1Key:(StimshopOkioByteString *)key __attribute__((swift_name("hmacSha1(key:)")));
- (StimshopOkioByteString *)hmacSha256Key:(StimshopOkioByteString *)key __attribute__((swift_name("hmacSha256(key:)")));
- (StimshopOkioByteString *)hmacSha512Key:(StimshopOkioByteString *)key __attribute__((swift_name("hmacSha512(key:)")));
- (int32_t)indexOfOther:(StimshopKotlinByteArray *)other fromIndex:(int32_t)fromIndex __attribute__((swift_name("indexOf(other:fromIndex:)")));
- (int32_t)indexOfOther:(StimshopOkioByteString *)other fromIndex_:(int32_t)fromIndex __attribute__((swift_name("indexOf(other:fromIndex_:)")));
- (int32_t)lastIndexOfOther:(StimshopKotlinByteArray *)other fromIndex:(int32_t)fromIndex __attribute__((swift_name("lastIndexOf(other:fromIndex:)")));
- (int32_t)lastIndexOfOther:(StimshopOkioByteString *)other fromIndex_:(int32_t)fromIndex __attribute__((swift_name("lastIndexOf(other:fromIndex_:)")));
- (StimshopOkioByteString *)md5 __attribute__((swift_name("md5()")));
- (BOOL)rangeEqualsOffset:(int32_t)offset other:(StimshopKotlinByteArray *)other otherOffset:(int32_t)otherOffset byteCount:(int32_t)byteCount __attribute__((swift_name("rangeEquals(offset:other:otherOffset:byteCount:)")));
- (BOOL)rangeEqualsOffset:(int32_t)offset other:(StimshopOkioByteString *)other otherOffset:(int32_t)otherOffset byteCount_:(int32_t)byteCount __attribute__((swift_name("rangeEquals(offset:other:otherOffset:byteCount_:)")));
- (StimshopOkioByteString *)sha1 __attribute__((swift_name("sha1()")));
- (StimshopOkioByteString *)sha256 __attribute__((swift_name("sha256()")));
- (StimshopOkioByteString *)sha512 __attribute__((swift_name("sha512()")));
- (BOOL)startsWithPrefix:(StimshopKotlinByteArray *)prefix __attribute__((swift_name("startsWith(prefix:)")));
- (BOOL)startsWithPrefix_:(StimshopOkioByteString *)prefix __attribute__((swift_name("startsWith(prefix_:)")));
- (StimshopOkioByteString *)substringBeginIndex:(int32_t)beginIndex endIndex:(int32_t)endIndex __attribute__((swift_name("substring(beginIndex:endIndex:)")));
- (StimshopOkioByteString *)toAsciiLowercase __attribute__((swift_name("toAsciiLowercase()")));
- (StimshopOkioByteString *)toAsciiUppercase __attribute__((swift_name("toAsciiUppercase()")));
- (StimshopKotlinByteArray *)toByteArray __attribute__((swift_name("toByteArray()")));
- (NSString *)description __attribute__((swift_name("description()")));
- (NSString *)utf8 __attribute__((swift_name("utf8()")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end

__attribute__((swift_name("OkioSource")))
@protocol StimshopOkioSource <StimshopOkioCloseable>
@required

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (int64_t)readSink:(StimshopOkioBuffer *)sink byteCount:(int64_t)byteCount error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("read(sink:byteCount:)"))) __attribute__((swift_error(nonnull_error)));
- (StimshopOkioTimeout *)timeout __attribute__((swift_name("timeout()")));
@end

__attribute__((swift_name("OkioBufferedSource")))
@protocol StimshopOkioBufferedSource <StimshopOkioSource>
@required
- (BOOL)exhausted __attribute__((swift_name("exhausted()")));
- (int64_t)indexOfB:(int8_t)b __attribute__((swift_name("indexOf(b:)")));
- (int64_t)indexOfB:(int8_t)b fromIndex:(int64_t)fromIndex __attribute__((swift_name("indexOf(b:fromIndex:)")));
- (int64_t)indexOfB:(int8_t)b fromIndex:(int64_t)fromIndex toIndex:(int64_t)toIndex __attribute__((swift_name("indexOf(b:fromIndex:toIndex:)")));
- (int64_t)indexOfBytes:(StimshopOkioByteString *)bytes __attribute__((swift_name("indexOf(bytes:)")));
- (int64_t)indexOfBytes:(StimshopOkioByteString *)bytes fromIndex:(int64_t)fromIndex __attribute__((swift_name("indexOf(bytes:fromIndex:)")));
- (int64_t)indexOfElementTargetBytes:(StimshopOkioByteString *)targetBytes __attribute__((swift_name("indexOfElement(targetBytes:)")));
- (int64_t)indexOfElementTargetBytes:(StimshopOkioByteString *)targetBytes fromIndex:(int64_t)fromIndex __attribute__((swift_name("indexOfElement(targetBytes:fromIndex:)")));
- (id<StimshopOkioBufferedSource>)peek __attribute__((swift_name("peek_()")));
- (BOOL)rangeEqualsOffset:(int64_t)offset bytes:(StimshopOkioByteString *)bytes __attribute__((swift_name("rangeEquals(offset:bytes:)")));
- (BOOL)rangeEqualsOffset:(int64_t)offset bytes:(StimshopOkioByteString *)bytes bytesOffset:(int32_t)bytesOffset byteCount:(int32_t)byteCount __attribute__((swift_name("rangeEquals(offset:bytes:bytesOffset:byteCount:)")));
- (int32_t)readSink:(StimshopKotlinByteArray *)sink __attribute__((swift_name("read(sink:)")));
- (int32_t)readSink:(StimshopKotlinByteArray *)sink offset:(int32_t)offset byteCount:(int32_t)byteCount __attribute__((swift_name("read(sink:offset:byteCount:)")));
- (int64_t)readAllSink:(id<StimshopOkioSink>)sink __attribute__((swift_name("readAll(sink:)")));
- (int8_t)readByte __attribute__((swift_name("readByte()")));
- (StimshopKotlinByteArray *)readByteArray __attribute__((swift_name("readByteArray()")));
- (StimshopKotlinByteArray *)readByteArrayByteCount:(int64_t)byteCount __attribute__((swift_name("readByteArray(byteCount:)")));
- (StimshopOkioByteString *)readByteString __attribute__((swift_name("readByteString()")));
- (StimshopOkioByteString *)readByteStringByteCount:(int64_t)byteCount __attribute__((swift_name("readByteString(byteCount:)")));
- (int64_t)readDecimalLong __attribute__((swift_name("readDecimalLong()")));
- (void)readFullySink:(StimshopKotlinByteArray *)sink __attribute__((swift_name("readFully(sink:)")));
- (void)readFullySink:(StimshopOkioBuffer *)sink byteCount:(int64_t)byteCount __attribute__((swift_name("readFully(sink:byteCount:)")));
- (int64_t)readHexadecimalUnsignedLong __attribute__((swift_name("readHexadecimalUnsignedLong()")));
- (int32_t)readInt __attribute__((swift_name("readInt()")));
- (int32_t)readIntLe __attribute__((swift_name("readIntLe()")));
- (int64_t)readLong __attribute__((swift_name("readLong()")));
- (int64_t)readLongLe __attribute__((swift_name("readLongLe()")));
- (int16_t)readShort __attribute__((swift_name("readShort()")));
- (int16_t)readShortLe __attribute__((swift_name("readShortLe()")));
- (NSString *)readUtf8 __attribute__((swift_name("readUtf8()")));
- (NSString *)readUtf8ByteCount:(int64_t)byteCount __attribute__((swift_name("readUtf8(byteCount:)")));
- (int32_t)readUtf8CodePoint __attribute__((swift_name("readUtf8CodePoint()")));
- (NSString * _Nullable)readUtf8Line __attribute__((swift_name("readUtf8Line()")));
- (NSString *)readUtf8LineStrict __attribute__((swift_name("readUtf8LineStrict()")));
- (NSString *)readUtf8LineStrictLimit:(int64_t)limit __attribute__((swift_name("readUtf8LineStrict(limit:)")));
- (BOOL)requestByteCount:(int64_t)byteCount __attribute__((swift_name("request(byteCount:)")));
- (void)requireByteCount:(int64_t)byteCount __attribute__((swift_name("require(byteCount:)")));
- (int32_t)selectOptions:(NSArray<StimshopOkioByteString *> *)options __attribute__((swift_name("select(options:)")));
- (void)skipByteCount:(int64_t)byteCount __attribute__((swift_name("skip(byteCount:)")));
@property (readonly) StimshopOkioBuffer *buffer __attribute__((swift_name("buffer")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OkioBuffer")))
@interface StimshopOkioBuffer : StimshopBase <StimshopOkioBufferedSource, StimshopOkioBufferedSink>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)clear __attribute__((swift_name("clear()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)closeAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("close()")));
- (int64_t)completeSegmentByteCount __attribute__((swift_name("completeSegmentByteCount()")));
- (StimshopOkioBuffer *)doCopy __attribute__((swift_name("doCopy()")));
- (StimshopOkioBuffer *)doCopyToOut:(StimshopOkioBuffer *)out offset:(int64_t)offset __attribute__((swift_name("doCopyTo(out:offset:)")));
- (StimshopOkioBuffer *)doCopyToOut:(StimshopOkioBuffer *)out offset:(int64_t)offset byteCount:(int64_t)byteCount __attribute__((swift_name("doCopyTo(out:offset:byteCount:)")));
- (StimshopOkioBuffer *)emit __attribute__((swift_name("emit()")));
- (StimshopOkioBuffer *)emitCompleteSegments __attribute__((swift_name("emitCompleteSegments()")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (BOOL)exhausted __attribute__((swift_name("exhausted()")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)flushAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("flush()")));
- (int8_t)getPos:(int64_t)pos __attribute__((swift_name("get(pos:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (StimshopOkioByteString *)hmacSha1Key:(StimshopOkioByteString *)key __attribute__((swift_name("hmacSha1(key:)")));
- (StimshopOkioByteString *)hmacSha256Key:(StimshopOkioByteString *)key __attribute__((swift_name("hmacSha256(key:)")));
- (StimshopOkioByteString *)hmacSha512Key:(StimshopOkioByteString *)key __attribute__((swift_name("hmacSha512(key:)")));
- (int64_t)indexOfB:(int8_t)b __attribute__((swift_name("indexOf(b:)")));
- (int64_t)indexOfB:(int8_t)b fromIndex:(int64_t)fromIndex __attribute__((swift_name("indexOf(b:fromIndex:)")));
- (int64_t)indexOfB:(int8_t)b fromIndex:(int64_t)fromIndex toIndex:(int64_t)toIndex __attribute__((swift_name("indexOf(b:fromIndex:toIndex:)")));
- (int64_t)indexOfBytes:(StimshopOkioByteString *)bytes __attribute__((swift_name("indexOf(bytes:)")));
- (int64_t)indexOfBytes:(StimshopOkioByteString *)bytes fromIndex:(int64_t)fromIndex __attribute__((swift_name("indexOf(bytes:fromIndex:)")));
- (int64_t)indexOfElementTargetBytes:(StimshopOkioByteString *)targetBytes __attribute__((swift_name("indexOfElement(targetBytes:)")));
- (int64_t)indexOfElementTargetBytes:(StimshopOkioByteString *)targetBytes fromIndex:(int64_t)fromIndex __attribute__((swift_name("indexOfElement(targetBytes:fromIndex:)")));
- (StimshopOkioByteString *)md5 __attribute__((swift_name("md5()")));
- (id<StimshopOkioBufferedSource>)peek __attribute__((swift_name("peek_()")));
- (BOOL)rangeEqualsOffset:(int64_t)offset bytes:(StimshopOkioByteString *)bytes __attribute__((swift_name("rangeEquals(offset:bytes:)")));
- (BOOL)rangeEqualsOffset:(int64_t)offset bytes:(StimshopOkioByteString *)bytes bytesOffset:(int32_t)bytesOffset byteCount:(int32_t)byteCount __attribute__((swift_name("rangeEquals(offset:bytes:bytesOffset:byteCount:)")));
- (int32_t)readSink:(StimshopKotlinByteArray *)sink __attribute__((swift_name("read(sink:)")));
- (int32_t)readSink:(StimshopKotlinByteArray *)sink offset:(int32_t)offset byteCount:(int32_t)byteCount __attribute__((swift_name("read(sink:offset:byteCount:)")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (int64_t)readSink:(StimshopOkioBuffer *)sink byteCount:(int64_t)byteCount error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("read(sink:byteCount:)"))) __attribute__((swift_error(nonnull_error)));
- (int64_t)readAllSink:(id<StimshopOkioSink>)sink __attribute__((swift_name("readAll(sink:)")));
- (StimshopOkioBufferUnsafeCursor *)readAndWriteUnsafeUnsafeCursor:(StimshopOkioBufferUnsafeCursor *)unsafeCursor __attribute__((swift_name("readAndWriteUnsafe(unsafeCursor:)")));
- (int8_t)readByte __attribute__((swift_name("readByte()")));
- (StimshopKotlinByteArray *)readByteArray __attribute__((swift_name("readByteArray()")));
- (StimshopKotlinByteArray *)readByteArrayByteCount:(int64_t)byteCount __attribute__((swift_name("readByteArray(byteCount:)")));
- (StimshopOkioByteString *)readByteString __attribute__((swift_name("readByteString()")));
- (StimshopOkioByteString *)readByteStringByteCount:(int64_t)byteCount __attribute__((swift_name("readByteString(byteCount:)")));
- (int64_t)readDecimalLong __attribute__((swift_name("readDecimalLong()")));
- (void)readFullySink:(StimshopKotlinByteArray *)sink __attribute__((swift_name("readFully(sink:)")));
- (void)readFullySink:(StimshopOkioBuffer *)sink byteCount:(int64_t)byteCount __attribute__((swift_name("readFully(sink:byteCount:)")));
- (int64_t)readHexadecimalUnsignedLong __attribute__((swift_name("readHexadecimalUnsignedLong()")));
- (int32_t)readInt __attribute__((swift_name("readInt()")));
- (int32_t)readIntLe __attribute__((swift_name("readIntLe()")));
- (int64_t)readLong __attribute__((swift_name("readLong()")));
- (int64_t)readLongLe __attribute__((swift_name("readLongLe()")));
- (int16_t)readShort __attribute__((swift_name("readShort()")));
- (int16_t)readShortLe __attribute__((swift_name("readShortLe()")));
- (StimshopOkioBufferUnsafeCursor *)readUnsafeUnsafeCursor:(StimshopOkioBufferUnsafeCursor *)unsafeCursor __attribute__((swift_name("readUnsafe(unsafeCursor:)")));
- (NSString *)readUtf8 __attribute__((swift_name("readUtf8()")));
- (NSString *)readUtf8ByteCount:(int64_t)byteCount __attribute__((swift_name("readUtf8(byteCount:)")));
- (int32_t)readUtf8CodePoint __attribute__((swift_name("readUtf8CodePoint()")));
- (NSString * _Nullable)readUtf8Line __attribute__((swift_name("readUtf8Line()")));
- (NSString *)readUtf8LineStrict __attribute__((swift_name("readUtf8LineStrict()")));
- (NSString *)readUtf8LineStrictLimit:(int64_t)limit __attribute__((swift_name("readUtf8LineStrict(limit:)")));
- (BOOL)requestByteCount:(int64_t)byteCount __attribute__((swift_name("request(byteCount:)")));
- (void)requireByteCount:(int64_t)byteCount __attribute__((swift_name("require(byteCount:)")));
- (int32_t)selectOptions:(NSArray<StimshopOkioByteString *> *)options __attribute__((swift_name("select(options:)")));
- (StimshopOkioByteString *)sha1 __attribute__((swift_name("sha1()")));
- (StimshopOkioByteString *)sha256 __attribute__((swift_name("sha256()")));
- (StimshopOkioByteString *)sha512 __attribute__((swift_name("sha512()")));
- (void)skipByteCount:(int64_t)byteCount __attribute__((swift_name("skip(byteCount:)")));
- (StimshopOkioByteString *)snapshot __attribute__((swift_name("snapshot()")));
- (StimshopOkioByteString *)snapshotByteCount:(int32_t)byteCount __attribute__((swift_name("snapshot(byteCount:)")));
- (StimshopOkioTimeout *)timeout __attribute__((swift_name("timeout()")));
- (NSString *)description __attribute__((swift_name("description()")));
- (StimshopOkioBuffer *)writeSource:(StimshopKotlinByteArray *)source __attribute__((swift_name("write(source:)")));
- (StimshopOkioBuffer *)writeSource:(StimshopKotlinByteArray *)source offset:(int32_t)offset byteCount:(int32_t)byteCount __attribute__((swift_name("write(source:offset:byteCount:)")));

/**
 * @note This method converts instances of IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)writeSource:(StimshopOkioBuffer *)source byteCount:(int64_t)byteCount error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("write(source:byteCount_:)")));
- (StimshopOkioBuffer *)writeByteString:(StimshopOkioByteString *)byteString __attribute__((swift_name("write(byteString:)")));
- (StimshopOkioBuffer *)writeByteString:(StimshopOkioByteString *)byteString offset:(int32_t)offset byteCount:(int32_t)byteCount __attribute__((swift_name("write(byteString:offset:byteCount:)")));
- (StimshopOkioBuffer *)writeSource:(id<StimshopOkioSource>)source byteCount:(int64_t)byteCount __attribute__((swift_name("write(source:byteCount:)")));
- (int64_t)writeAllSource:(id<StimshopOkioSource>)source __attribute__((swift_name("writeAll(source:)")));
- (StimshopOkioBuffer *)writeByteB:(int32_t)b __attribute__((swift_name("writeByte(b:)")));
- (StimshopOkioBuffer *)writeDecimalLongV:(int64_t)v __attribute__((swift_name("writeDecimalLong(v:)")));
- (StimshopOkioBuffer *)writeHexadecimalUnsignedLongV:(int64_t)v __attribute__((swift_name("writeHexadecimalUnsignedLong(v:)")));
- (StimshopOkioBuffer *)writeIntI:(int32_t)i __attribute__((swift_name("writeInt(i:)")));
- (StimshopOkioBuffer *)writeIntLeI:(int32_t)i __attribute__((swift_name("writeIntLe(i:)")));
- (StimshopOkioBuffer *)writeLongV:(int64_t)v __attribute__((swift_name("writeLong(v:)")));
- (StimshopOkioBuffer *)writeLongLeV:(int64_t)v __attribute__((swift_name("writeLongLe(v:)")));
- (StimshopOkioBuffer *)writeShortS:(int32_t)s __attribute__((swift_name("writeShort(s:)")));
- (StimshopOkioBuffer *)writeShortLeS:(int32_t)s __attribute__((swift_name("writeShortLe(s:)")));
- (StimshopOkioBuffer *)writeUtf8String:(NSString *)string __attribute__((swift_name("writeUtf8(string:)")));
- (StimshopOkioBuffer *)writeUtf8String:(NSString *)string beginIndex:(int32_t)beginIndex endIndex:(int32_t)endIndex __attribute__((swift_name("writeUtf8(string:beginIndex:endIndex:)")));
- (StimshopOkioBuffer *)writeUtf8CodePointCodePoint:(int32_t)codePoint __attribute__((swift_name("writeUtf8CodePoint(codePoint:)")));
@property (readonly) StimshopOkioBuffer *buffer __attribute__((swift_name("buffer")));
@property (readonly) int64_t size __attribute__((swift_name("size")));
@end

__attribute__((swift_name("OkioTimeout")))
@interface StimshopOkioTimeout : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) StimshopOkioTimeoutCompanion *companion __attribute__((swift_name("companion")));
@end

__attribute__((swift_name("Apollo_apiCustomTypeValue")))
@interface StimshopApollo_apiCustomTypeValue<T> : StimshopBase
@property (class, readonly, getter=companion) StimshopApollo_apiCustomTypeValueCompanion *companion __attribute__((swift_name("companion")));
@property (readonly) T _Nullable value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiDeferredFragmentIdentifier")))
@interface StimshopApollo_apiDeferredFragmentIdentifier : StimshopBase
- (instancetype)initWithPath:(NSArray<id> *)path label:(NSString * _Nullable)label __attribute__((swift_name("init(path:label:)"))) __attribute__((objc_designated_initializer));
- (StimshopApollo_apiDeferredFragmentIdentifier *)doCopyPath:(NSArray<id> *)path label:(NSString * _Nullable)label __attribute__((swift_name("doCopy(path:label:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable label __attribute__((swift_name("label")));
@property (readonly) NSArray<id> *path __attribute__((swift_name("path")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModuleCollector")))
@protocol StimshopKotlinx_serialization_coreSerializersModuleCollector
@required
- (void)contextualKClass:(id<StimshopKotlinKClass>)kClass provider:(id<StimshopKotlinx_serialization_coreKSerializer> (^)(NSArray<id<StimshopKotlinx_serialization_coreKSerializer>> *))provider __attribute__((swift_name("contextual(kClass:provider:)")));
- (void)contextualKClass:(id<StimshopKotlinKClass>)kClass serializer:(id<StimshopKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<StimshopKotlinKClass>)baseClass actualClass:(id<StimshopKotlinKClass>)actualClass actualSerializer:(id<StimshopKotlinx_serialization_coreKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
- (void)polymorphicDefaultBaseClass:(id<StimshopKotlinKClass>)baseClass defaultDeserializerProvider:(id<StimshopKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefault(baseClass:defaultDeserializerProvider:)")));
- (void)polymorphicDefaultDeserializerBaseClass:(id<StimshopKotlinKClass>)baseClass defaultDeserializerProvider:(id<StimshopKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefaultDeserializer(baseClass:defaultDeserializerProvider:)")));
- (void)polymorphicDefaultSerializerBaseClass:(id<StimshopKotlinKClass>)baseClass defaultSerializerProvider:(id<StimshopKotlinx_serialization_coreSerializationStrategy> _Nullable (^)(id))defaultSerializerProvider __attribute__((swift_name("polymorphicDefaultSerializer(baseClass:defaultSerializerProvider:)")));
@end

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol StimshopKotlinKDeclarationContainer
@required
@end

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol StimshopKotlinKAnnotatedElement
@required
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.1")
*/
__attribute__((swift_name("KotlinKClassifier")))
@protocol StimshopKotlinKClassifier
@required
@end

__attribute__((swift_name("KotlinKClass")))
@protocol StimshopKotlinKClass <StimshopKotlinKDeclarationContainer, StimshopKotlinKAnnotatedElement, StimshopKotlinKClassifier>
@required

/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.1")
*/
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end

__attribute__((swift_name("KotlinByteIterator")))
@interface StimshopKotlinByteIterator : StimshopBase <StimshopKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (StimshopByte *)next __attribute__((swift_name("next()")));
- (int8_t)nextByte __attribute__((swift_name("nextByte()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OkioByteString.Companion")))
@interface StimshopOkioByteStringCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopOkioByteStringCompanion *shared __attribute__((swift_name("shared")));
- (StimshopOkioByteString *)ofData:(StimshopKotlinByteArray *)data __attribute__((swift_name("of(data:)")));
- (StimshopOkioByteString * _Nullable)decodeBase64:(NSString *)receiver __attribute__((swift_name("decodeBase64(_:)")));
- (StimshopOkioByteString *)decodeHex:(NSString *)receiver __attribute__((swift_name("decodeHex(_:)")));
- (StimshopOkioByteString *)encodeUtf8:(NSString *)receiver __attribute__((swift_name("encodeUtf8(_:)")));
- (StimshopOkioByteString *)toByteString:(StimshopKotlinByteArray *)receiver offset:(int32_t)offset byteCount:(int32_t)byteCount __attribute__((swift_name("toByteString(_:offset:byteCount:)")));
@property (readonly) StimshopOkioByteString *EMPTY __attribute__((swift_name("EMPTY")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OkioBuffer.UnsafeCursor")))
@interface StimshopOkioBufferUnsafeCursor : StimshopBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)close __attribute__((swift_name("close()")));
- (int64_t)expandBufferMinByteCount:(int32_t)minByteCount __attribute__((swift_name("expandBuffer(minByteCount:)")));
- (int32_t)next __attribute__((swift_name("next()")));
- (int64_t)resizeBufferNewSize:(int64_t)newSize __attribute__((swift_name("resizeBuffer(newSize:)")));
- (int32_t)seekOffset:(int64_t)offset __attribute__((swift_name("seek(offset:)")));
@property StimshopOkioBuffer * _Nullable buffer __attribute__((swift_name("buffer")));
@property StimshopKotlinByteArray * _Nullable data __attribute__((swift_name("data")));
@property int32_t end __attribute__((swift_name("end")));
@property int64_t offset __attribute__((swift_name("offset")));
@property BOOL readWrite __attribute__((swift_name("readWrite")));
@property int32_t start __attribute__((swift_name("start")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OkioTimeout.Companion")))
@interface StimshopOkioTimeoutCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopOkioTimeoutCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) StimshopOkioTimeout *NONE __attribute__((swift_name("NONE")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Apollo_apiCustomTypeValueCompanion")))
@interface StimshopApollo_apiCustomTypeValueCompanion : StimshopBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) StimshopApollo_apiCustomTypeValueCompanion *shared __attribute__((swift_name("shared")));

/**
 * @note annotations
 *   kotlin.jvm.JvmStatic
*/
- (StimshopApollo_apiCustomTypeValue<id> *)fromRawValueValue:(id _Nullable)value __attribute__((swift_name("fromRawValue(value:)")));
@end

#pragma pop_macro("_Nullable_result")
#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
