//
//  MainViewController.swift
//  iosApp
//
//  Created by Frédéric Bellano on 31/03/2021.
//  Copyright © 2021 orgName. All rights reserved.
//

import UIKit
import stimshop
import AVFoundation

class MainViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private var events = [String]()
    var player: AVAudioPlayer?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.navigationController?.title = "SDK Commons"
        initSDK()

        self.tableView.delegate = self
        self.tableView.dataSource = self

        sswitch.isOn = ResourcesConfiguration().detectionMode == DetectionMode.wius

        // Do any additional setup after loading the view.
    }
    
    deinit{
        StimShop.companion.get().stopDetection()
    }

    private func initSDK() {
        let stimshopSDk = try! StimShop.Builder().debugEnabled(debug: false).extraMessage(message: "Demo IOS - sent message when a signal is detected").build()

             let df = DateFormatter()
             df.dateFormat = "HH:mm:ss.SSS"

             StimshopBus().onStimshopEvent(observer: self) {[weak self] (event) in
                 let formattedDate = df.string(from: Date())
                 switch (event) {
                 case is StimShopReady:
                     self?.events.append("\(formattedDate) - SDK isReady = \((event as! StimShopReady).isReady)")
                     StimShop.companion.get().startDetection()
                     break
                 case is NoAuthorizedSignals:
                     self?.events.append("\(formattedDate) - \((event as! NoAuthorizedSignals).message)")
                     break
                 case is MissingPermissions:
                     self?.events.append("\(formattedDate) - SDK Missing permissions - \((event as! MissingPermissions).permissions)")
                     self?.requestPermissions()
                 case is StimShopDetectChirp:
                     self?.events.append("\(formattedDate) - Chirp detecté sur le channel \((event as! StimShopDetectChirp).channelId)")
                 case is StimShopDetectCode:
                     StimShop.companion.get().stopDetection()
                     self?.events.append("\(formattedDate) - Code décodé \((event as! StimShopDetectCode).code)")
                     self?.playSound()
                 case is StimShopDetectNotification:
                     self?.events.append("\(formattedDate) - Notification  \((event as! StimShopDetectNotification).item.id) \((event as! StimShopDetectNotification).notification.displayName)")
                 case is WiUSChecksumError:
                     self?.events.append("\(formattedDate) - Checksum error : \((event as! WiUSChecksumError).message)")
                 case is DetectionStarted, is DetectionStopped:
                     break;
                 default:
                     self?.events.append("\(formattedDate) - Default")
                 }
                 self?.tableView.reloadData()
             }
    }


    @IBAction func onClearButtonPushed(_ sender: Any) {
        self.events.removeAll()
        self.tableView.reloadData()
    }
    
    private func requestPermissions() {
        if (AVAudioSession.sharedInstance().responds(to: #selector(AVAudioSession.requestRecordPermission(_:)))) {
            AVAudioSession.sharedInstance().requestRecordPermission { (granted) in
                if (granted) {
                    print("Microphone is enabled")
                    DispatchQueue.main.async {
                        StimShop.companion.get().restart()
                    }
                } else {

                    DispatchQueue.main.async {
                        let alert = UIAlertController.init(title: "Erreur", message: "Le microphone n'est pas accessible. Rendez-le accessible depuis les réglages de l'iPhone", preferredStyle: .alert)

                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                        }))

                        self.present(alert, animated: true)
                    }
                }
            }
        }
    }

    @IBOutlet weak var sswitch: UISwitch!
    @IBAction func onValueChanged(_ sender: Any) {
        if let sswitch = sender as? UISwitch {

            if !sswitch.isOn {
                ResourcesConfiguration().detectionMode = DetectionMode.ucheckin
            } else {
                ResourcesConfiguration().detectionMode = DetectionMode.wius
            }
            events.removeAll()
            tableView.reloadData()
            StimShop.Companion().get().restart()
        }
    }
    
    func playSound() {
        let url = Bundle.main.url(forResource: "test", withExtension: ".mp3")!
        do {
       
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, options: .mixWithOthers)
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            player.delegate = self
            player.prepareToPlay()
            player.play()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.events.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.events.reversed()[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell") {
            cell.textLabel?.text = item
            cell.textLabel?.numberOfLines = 0
            return cell
        } else {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "eventCell")
            cell.textLabel?.text = item
            cell.textLabel?.numberOfLines = 0
            return cell
        }

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}

extension MainViewController : AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
            player.stop()
            self.player = nil
        }
        catch {
            print(error.localizedDescription)

        }
        StimShop.companion.get().restart()
    }
}

class EventCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
}
